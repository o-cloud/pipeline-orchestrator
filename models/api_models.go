package models

import (
	"fmt"

	"github.com/argoproj/argo-workflows/v3/pkg/apis/workflow/v1alpha1"
	"gitlab.com/o-cloud/cwl"
	values "gitlab.com/o-cloud/pipeline-orchestrator/cwl-values"
	"gitlab.com/o-cloud/pipeline-orchestrator/graph"
	"gitlab.com/o-cloud/pipeline-orchestrator/graph/parallel"
)

type StartWorkflowResponse struct {
	Namespace string            `json:"namespace"`
	Workflow  v1alpha1.Workflow `json:"workflow"`
}

type ComputeProvider struct {
	EndpointLocation string `json:"endpointLocation"`
	IsLocal          bool   `json:"isLocal"`
	Metadata         struct {
		KubernetesApi string        `json:"kubernetesApi"`
		S3            S3Credentials `json:"s3"`
	} `json:"metadata"`
}

type S3Credentials struct {
	AccessKey string `json:"accessKey"`
	SecretKey string `json:"secretKey"`
}

type StartWorkflowRequest struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	Blocks []struct {
		Index   int    `json:"index"`
		Name    string `json:"name"`
		Payload struct {
			Cwl              cwl.CommandLineTool
			Parameters       map[string]interface{} `json:"parameters"`
			ProviderType     string                 `json:"providerType"`
			EndpointLocation string                 `json:"endpointLocation"`
			ComputeProvider  ComputeProvider        `json:"computeProvider"`
			S3Provider       ComputeProvider        `json:"s3Provider" bson:"s3Provider"`
		} `json:"payload"`
	} `json:"blocks"`
	Links []struct {
		ToIndex       int    `json:"toIndex"`
		PortFromIndex int    `json:"portFromIndex"`
		PortToIndex   int    `json:"portToIndex"`
		FromIndex     int    `json:"fromIndex"`
		FromName      string `json:"fromName"`
		ToName        string `json:"toName"`
	} `json:"links"`
}

func (request *StartWorkflowRequest) ToGraph() (*graph.Graph, error) {
	nodes := make([]*graph.Node, len(request.Blocks))
	links := make([]*graph.Link, len(request.Links))
	nodesMap := make(map[int]*graph.Node, len(request.Blocks))
	for i, block := range request.Blocks {
		node := graph.Node{
			BlockIndex:       block.Index,
			Cwl:              block.Payload.Cwl,
			Inputs:           make(map[string]values.Value, len(block.Payload.Parameters)),
			ProviderType:     block.Payload.ProviderType,
			EndpointLocation: block.Payload.EndpointLocation,
		}
		for key, value := range block.Payload.Parameters {
			input, err := parseParameter(key, value, node.Cwl)
			if err != nil {
				return nil, fmt.Errorf("cannot parse block %s with index %d inputs, %s", block.Name, block.Index, err)
			}
			node.Inputs[key] = input
		}
		nodes[i] = &node
		nodesMap[block.Index] = &node
	}
	for i, l := range request.Links {
		from, ok := nodesMap[l.FromIndex]
		if !ok {
			return nil, fmt.Errorf("cannot parse link %+v, cannot find block with index %d", l, l.FromIndex)
		}
		to, ok := nodesMap[l.ToIndex]
		if !ok {
			return nil, fmt.Errorf("cannot parse link %+v, cannot find block with index %d", l, l.ToIndex)
		}
		link := graph.Link{
			From:     from,
			To:       to,
			OutputID: l.FromName,
			InputID:  l.ToName,
		}
		links[i] = &link
	}
	return graph.NewGraph(nodes, links)
}

func parseParameter(name string, value interface{}, c cwl.CommandLineTool) (values.Value, error) {
	cwlParam, ok := c.Inputs[name]
	if !ok {
		return nil, fmt.Errorf("cannot parse parameter %s, cannot find it in cwl", name)
	}
	inputValue, err := values.ParseInputValue(value, cwlParam.Type.Wrapped)
	if err != nil {
		return nil, fmt.Errorf("cannot parse parameter %s, %s", name, err)
	}
	return inputValue, nil
}

func (request *StartWorkflowRequest) ToParallelGraph() (*parallel.Graph, error) {
	nodes := make([]*parallel.Node, len(request.Blocks))
	links := make([]*parallel.Link, len(request.Links))
	nodesMap := make(map[int]*parallel.Node, len(request.Blocks))
	for i, block := range request.Blocks {
		node := parallel.Node{
			BlockIndex:       block.Index,
			Cwl:              block.Payload.Cwl,
			Inputs:           make(map[string]values.Value, len(block.Payload.Parameters)),
			ProviderType:     block.Payload.ProviderType,
			EndpointLocation: block.Payload.EndpointLocation,
		}
		for key, value := range block.Payload.Parameters {
			input, err := parseParameter(key, value, node.Cwl)
			if err != nil {
				return nil, fmt.Errorf("cannot parse block %s with index %d inputs, %s", block.Name, block.Index, err)
			}
			node.Inputs[key] = input
		}
		nodes[i] = &node
		nodesMap[block.Index] = &node
	}
	for i, l := range request.Links {
		from, ok := nodesMap[l.FromIndex]
		if !ok {
			return nil, fmt.Errorf("cannot parse link %+v, cannot find block with index %d", l, l.FromIndex)
		}
		to, ok := nodesMap[l.ToIndex]
		if !ok {
			return nil, fmt.Errorf("cannot parse link %+v, cannot find block with index %d", l, l.ToIndex)
		}
		link := parallel.Link{
			From:     from,
			To:       to,
			OutputID: l.FromName,
			InputID:  l.ToName,
		}
		links[i] = &link
	}
	return parallel.NewGraph(nodes, links)
}
