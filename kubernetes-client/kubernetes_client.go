package kubernetes_client

import (
	"context"
	"fmt"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

func CreateSecret(namespace string, secret v1.Secret) error {
	client, err := createClient()
	if err != nil {
		return fmt.Errorf("cannot create kube client, %s", err)
	}
	_, err = client.CoreV1().Secrets(namespace).Create(context.TODO(), &secret, metav1.CreateOptions{})
	if err != nil {
		return fmt.Errorf("cannot create secret, %s", err)
	}
	return nil
}

func GetPodList(namespace string) (*v1.PodList, error) {
	client, err := createClient()
	if err != nil {
		return nil, fmt.Errorf("cannot get kubernetes podList, %s", err)
	}
	list, err := client.CoreV1().Pods(namespace).List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		return nil, fmt.Errorf("cannot get kubernetes podList, %s", err)
	}
	return list, nil
}

func createClient() (*kubernetes.Clientset, error) {
	config, err := rest.InClusterConfig()
	if err != nil {
		return nil, fmt.Errorf("cannot create kube config, %s", err)
	}
	client, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, fmt.Errorf("cannot create kube client, %s", err)
	}
	return client, nil
}
