package data_provider_client

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"strings"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	values "gitlab.com/o-cloud/pipeline-orchestrator/cwl-values"
)

type dataInserterClient struct {
}

func NewDataInserterClient() DataProviderClient {
	return &dataInserterClient{}
}

func (c *dataInserterClient) ReserveData(url string, inputs map[string]values.Value) (map[string]values.Value, error) {
	logrus.Debugf("sending request to data provider %s", url)
	body := make(map[string]interface{})
	for key, value := range inputs {
		body[key] = value.GetValue()
	}
	jsonBody, err := json.Marshal(body)
	if err != nil {
		return nil, errors.Wrap(err, "cannot marshal json")
	}
	client := http.Client{}
	r, err := client.Post(url, "application/json", bytes.NewReader(jsonBody))
	if err != nil {
		return nil, err
	}
	rBytes, err := io.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}
	var response map[string]json.RawMessage
	err = json.Unmarshal(rBytes, &response)
	if err != nil {
		return nil, err
	}
	result := make(map[string]values.Value)
	for key, value := range response {
		strValue := string(value)
		strValue = strings.ReplaceAll(strValue, "%", "%"+"%")
		result[key] = values.StringValue{Value: strValue}
	}
	logrus.Debugf("response from data provider %s handled successfully", url)
	return result, nil
}
