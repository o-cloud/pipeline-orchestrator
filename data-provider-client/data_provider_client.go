package data_provider_client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/sirupsen/logrus"
	values "gitlab.com/o-cloud/pipeline-orchestrator/cwl-values"
	"io"
	"net/http"
	"time"
)

type DataProviderClient interface {
	ReserveData(url string, inputs map[string]values.Value) (map[string]values.Value, error)
}

type client struct {
}

func New() DataProviderClient {
	return &client{}
}

func (c *client) ReserveData(url string, inputs map[string]values.Value) (map[string]values.Value, error) {
	logrus.Debugf("sending request to data provider %s", url)
	body := make(map[string]interface{}, 0)
	for key, value := range inputs {
		body[key] = value.GetValue()
	}
	jsonBody, err := json.Marshal(body)
	if err != nil {
		return nil, fmt.Errorf("cannot marshal json")
	}
	client := http.Client{}
	r, err := client.Post(url, "application/json", bytes.NewReader(jsonBody))
	if err != nil {
		return nil, err
	}
	rBytes, err := io.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}
	var response DataProviderResponse
	err = json.Unmarshal(rBytes, &response)
	if err != nil {
		return nil, err
	}
	urls := make([]string, len(response.Images))
	for i, image := range response.Images {
		urls[i] = "\"" + image.URL + "\""
	}
	result := make(map[string]values.Value, 1)
	result["url"] = values.StringValue{Value: urls[0]}
	logrus.Debugf("response from data provider %s handled successfully", url)
	return result, nil
}

type DataProviderResponse struct {
	Macaroon string `json:"Macaroon"`
	Images   []struct {
		Query struct {
			Time        time.Time `json:"Time"`
			BoundingBox []float64 `json:"BoundingBox"`
		} `json:"Query"`
		URL string `json:"URL"`
	} `json:"Images"`
}
