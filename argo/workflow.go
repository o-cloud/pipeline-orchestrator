package argo

import (
	argoLib "github.com/argoproj/argo-workflows/v3/pkg/apis/workflow/v1alpha1"
)

func NewWorkflow(templates *[]argoLib.Template, entryPoint string) *argoLib.Workflow {
	w := new(argoLib.Workflow)
	w.APIVersion = "argoproj.io/v1alpha1"
	w.Kind = "Workflow"
	w.GenerateName = "workflow-"
	w.Spec = argoLib.WorkflowSpec{
		Entrypoint:         entryPoint,
		Templates:          *templates,
		ServiceAccountName: "argo-workflow",
	}
	return w
}
