package compute_provider_client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	kubernetes_client "gitlab.com/o-cloud/pipeline-orchestrator/kubernetes-client"
	"gitlab.com/o-cloud/pipeline-orchestrator/models"
	"net/http"
	"strings"
	"time"
)

type prepareLocalClusterRequest struct {
	TargetNamespace string `json:"targetNamespace"`
}

func PrepareLocalClusterBeforeExecution(namespace string) error {
	logrus.Debugln("sending prepare local cluster request to compute provider")
	url := fmt.Sprintf("%s/api/compute/prepareLocalCluster", viper.GetString("COMPUTE_PROVIDER_API"))
	body := prepareLocalClusterRequest{
		TargetNamespace: namespace,
	}
	payloadBuf := new(bytes.Buffer)
	if err := json.NewEncoder(payloadBuf).Encode(body); err != nil {
		logrus.Debugf("error while encoding body %+v", body)
		return fmt.Errorf("error while sending prepare local cluster request to compute provider, %s", err)
	}
	client := http.Client{}
	r, err := client.Post(url, "application/json", payloadBuf)
	if err != nil {
		return fmt.Errorf("error while sending prepare local cluster request to compute provider, %s", err)
	}
	if r.StatusCode != 200 {
		return fmt.Errorf("error while sending prepare local cluster request to compute provider, compute provider responded with status %d", r.StatusCode)
	}
	logrus.Debugf("compute provider responded to prepare local cluster request with status, %d", r.StatusCode)
	return nil
}

func Book(computeProviders []models.ComputeProvider, namespace string, identity string) {
	if len(computeProviders) == 0 || (len(computeProviders) == 1 && computeProviders[0].IsLocal) {
		logrus.Debugln("No booking necessary")
		return
	}
	logrus.Debugln("Booking with compute provider")
	for _, cp := range computeProviders {
		targetComputeProvider := cp.EndpointLocation
		targetKubeApi := cp.Metadata.KubernetesApi
		if !strings.HasSuffix(targetComputeProvider, "/") {
			targetComputeProvider += "/"
		}
		localProvider := fmt.Sprintf("%s/api/compute/token", viper.GetString("COMPUTE_PROVIDER_API"))
		body := make(map[string]interface{}, 0)
		body["tokenRequestURL"] = fmt.Sprintf("%stoken?identity=%s&namespace=%s", targetComputeProvider, identity, namespace)
		body["remoteKubernetesApiURL"] = targetKubeApi
		body["namespace"] = namespace
		jsonBody, err := json.Marshal(body)
		if err != nil {
			logrus.Fatalf("cannot marshal json, %s\n", err)
			return
		}
		client := http.Client{}
		r, err := client.Post(localProvider, "application/json", bytes.NewReader(jsonBody))
		if err != nil {
			logrus.Errorf("error when posting to compute provider, %s\n", err)
			return
		}
		if r.StatusCode != 200 {
			logrus.Errorf("error in response, status %d, %+v\n", r.StatusCode, r)
		}
	}
	waitForMulticlusterScheduler()
}

func waitForMulticlusterScheduler() {
	delay := 10 * time.Second
	totalTime := 0 * time.Second
	timeout := 60 * time.Second
	for {
		time.Sleep(delay)
		pods, err := kubernetes_client.GetPodList("admiralty")
		if err != nil {
			logrus.Debugf("cannot list admiralty pods, multicluster scheduler might not be ready, %s", err)
		}
		controllerReady := false
		schedulerReady := false
		for _, pod := range pods.Items {
			if strings.HasPrefix(pod.Name, "admiralty-multicluster-scheduler-controller-manager") && pod.Status.Phase == "Running" {
				controllerReady = true
			}
			if strings.HasPrefix(pod.Name, "admiralty-multicluster-scheduler-proxy-scheduler") && pod.Status.Phase == "Running" {
				schedulerReady = true
			}
			if controllerReady && schedulerReady {
				logrus.Debugln("multicluster scheduler is ready")
				time.Sleep(delay) //give it more time to start
				return
			}
		}
		totalTime += delay
		if totalTime > timeout {
			logrus.Errorln("timeout expired while waiting for multicluster scheduler, the execution will continue but multicluster scheduler might not be ready to execute the workload")
			return
		} else {
			logrus.Debugf("waiting for mulitcluster scheduler to be ready (%d sec elapsed)\n", totalTime/time.Second)
		}
	}
}
