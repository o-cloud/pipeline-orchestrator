package cwl_values

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/o-cloud/cwl"
	"testing"
)

func TestParseString(t *testing.T) {
	s, err := ParseInputValue("str", cwl.StringType{})
	assert.Nil(t, err)
	assert.IsType(t, StringValue{}, s)
	assert.Equal(t, "str", s.GetValue())

	s, err = ParseInputValue(42, cwl.StringType{})
	assert.NotNil(t, err)
}

func TestParseInt(t *testing.T) {
	i, err := ParseInputValue(42, cwl.IntegerType{})
	assert.Nil(t, err)
	assert.IsType(t, IntValue{}, i)
	assert.Equal(t, 42, i.GetValue())

	i, err = ParseInputValue("42", cwl.IntegerType{})
	assert.Nil(t, err)
	assert.IsType(t, IntValue{}, i)
	assert.Equal(t, 42, i.GetValue())

	i, err = ParseInputValue(42.42, cwl.IntegerType{})
	assert.NotNil(t, err)
}

func TestParseLong(t *testing.T) {
	l, err := ParseInputValue(42, cwl.LongType{})
	assert.Nil(t, err)
	assert.IsType(t, LongValue{}, l)
	assert.Equal(t, int64(42), l.GetValue())

	l, err = ParseInputValue("42", cwl.LongType{})
	assert.Nil(t, err)
	assert.IsType(t, LongValue{}, l)
	assert.Equal(t, int64(42), l.GetValue())

	l, err = ParseInputValue(42.42, cwl.LongType{})
	assert.NotNil(t, err)
}

func TestParseFloat(t *testing.T) {
	f, err := ParseInputValue(42.42, cwl.FloatType{})
	assert.Nil(t, err)
	assert.IsType(t, FloatValue{}, f)
	assert.Equal(t, float32(42.42), f.GetValue())

	f, err = ParseInputValue("42.42", cwl.FloatType{})
	assert.Nil(t, err)
	assert.IsType(t, FloatValue{}, f)
	assert.Equal(t, float32(42.42), f.GetValue())

	f, err = ParseInputValue(42, cwl.FloatType{})
	assert.Nil(t, err)
	assert.IsType(t, FloatValue{}, f)
	assert.Equal(t, float32(42), f.GetValue())

	f, err = ParseInputValue("42", cwl.FloatType{})
	assert.Nil(t, err)
	assert.IsType(t, FloatValue{}, f)
	assert.Equal(t, float32(42), f.GetValue())

	f, err = ParseInputValue("str", cwl.FloatType{})
	assert.NotNil(t, err)
}

func TestParseDouble(t *testing.T) {
	d, err := ParseInputValue(42.42, cwl.DoubleType{})
	assert.Nil(t, err)
	assert.IsType(t, DoubleValue{}, d)
	assert.Equal(t, 42.42, d.GetValue())

	d, err = ParseInputValue("42.42", cwl.DoubleType{})
	assert.Nil(t, err)
	assert.IsType(t, DoubleValue{}, d)
	assert.Equal(t, 42.42, d.GetValue())

	d, err = ParseInputValue(42, cwl.DoubleType{})
	assert.Nil(t, err)
	assert.IsType(t, DoubleValue{}, d)
	assert.Equal(t, float64(42), d.GetValue())

	d, err = ParseInputValue("42", cwl.DoubleType{})
	assert.Nil(t, err)
	assert.IsType(t, DoubleValue{}, d)
	assert.Equal(t, float64(42), d.GetValue())

	d, err = ParseInputValue("str", cwl.DoubleType{})
	assert.NotNil(t, err)
}

func TestParseBoolean(t *testing.T) {
	b, err := ParseInputValue(true, cwl.BooleanType{})
	assert.Nil(t, err)
	assert.IsType(t, BooleanValue{}, b)
	assert.Equal(t, true, b.GetValue())

	b, err = ParseInputValue(42, cwl.BooleanType{})
	assert.NotNil(t, err)
}

func TestParseArray(t *testing.T) {
	arrayType := cwl.CommandInputArraySchema{
		Items: cwl.CommandInputTypeWrapper{
			Wrapped: cwl.IntegerType{},
		},
	}

	v := []interface{}{map[string]interface{}{"value": 4}, map[string]interface{}{"value": 2}}
	a, err := ParseInputValue(v, arrayType)
	assert.Nil(t, err)
	assert.IsType(t, ArrayValue{}, a)
	assert.Equal(t, []interface{}{4, 2}, a.GetValue())

	a, err = ParseInputValue([]interface{}{map[string]interface{}{"value": 4}, map[string]interface{}{"value": 2}, map[string]interface{}{"value": 42.42}}, arrayType)
	assert.NotNil(t, err)
}

func TestParseEnum(t *testing.T) {
	enumType := cwl.CommandInputEnumSchema{}
	enumType.Symbols = []string{"v1", "v2"}

	e, err := ParseInputValue("v1", enumType)
	assert.Nil(t, err)
	assert.IsType(t, EnumValue{}, e)
	assert.Equal(t, "v1", e.GetValue())

	e, err = ParseInputValue("unknown", enumType)
	assert.NotNil(t, err)

	e, err = ParseInputValue(42, enumType)
	assert.NotNil(t, err)
}

func TestParseUnion(t *testing.T) {
	unionType := cwl.CommandInputUnionSchema{
		Types: []cwl.CommandInputTypeWrapper{
			{Wrapped: cwl.IntegerType{}},
			{Wrapped: cwl.StringType{}},
		},
	}

	u, err := ParseInputValue("str", unionType)
	assert.Nil(t, err)
	assert.IsType(t, StringValue{}, u)
	assert.Equal(t, "str", u.GetValue())

	u, err = ParseInputValue(42, unionType)
	assert.Nil(t, err)
	assert.IsType(t, IntValue{}, u)
	assert.Equal(t, 42, u.GetValue())

	u, err = ParseInputValue(42.42, unionType)
	assert.NotNil(t, err)
}

func TestParseRecord(t *testing.T) {
	fields := make(map[string]cwl.CommandInputRecordField, 2)
	fields["lat"] = cwl.CommandInputRecordField{
		Type: cwl.CommandInputTypeWrapper{
			Wrapped: cwl.FloatType{},
		},
	}
	fields["long"] = cwl.CommandInputRecordField{
		Type: cwl.CommandInputTypeWrapper{
			Wrapped: cwl.FloatType{},
		},
	}
	recordType := cwl.CommandInputRecordSchema{
		Fields: fields,
	}

	r, err := ParseInputValue(map[string]interface{}{"lat": 4.2, "long": 2.0}, recordType)
	assert.Nil(t, err)
	assert.IsType(t, RecordValue{}, r)
	m := r.GetValue()
	assert.IsType(t, map[string]interface{}{}, m)
	assert.Equal(t, float32(4.2), m.(map[string]interface{})["lat"])
	assert.Equal(t, float32(2.0), m.(map[string]interface{})["long"])

	r, err = ParseInputValue(map[string]interface{}{"lat": 4.2, "wrong": 2.0}, recordType)
	assert.NotNil(t, err)

	r, err = ParseInputValue(map[string]interface{}{"lat": 4.2}, recordType)
	assert.NotNil(t, err)

	r, err = ParseInputValue(map[string]interface{}{}, recordType)
	assert.NotNil(t, err)
}

func TestParseDateRange(t *testing.T) {
	startStr := "2020-08-22T00:00:00Z"
	endStr := "2020-09-22T00:00:00Z"
	fields := make(map[string]interface{})
	fields["start"] = startStr
	fields["end"] = endStr

	value, err := ParseInputValue(fields, cwl.DateRangeType{})
	assert.Nil(t, err)
	assert.IsType(t, DateRangeValue{}, value)
	assert.IsType(t, DateRange{}, value.GetValue())
	assert.Equal(t, startStr, value.GetValue().(DateRange).Start)
	assert.Equal(t, endStr, value.GetValue().(DateRange).End)

	_, err = ParseInputValue(map[string]interface{}{}, cwl.DateRangeType{})
	assert.NotNil(t, err)

	_, err = ParseInputValue(map[string]interface{}{
		"start": "12",
		"end":   "14",
	}, cwl.DateRangeType{})
	assert.NotNil(t, err)

	_, err = ParseInputValue(map[string]interface{}{
		"start": startStr,
	}, cwl.DateRangeType{})
	assert.NotNil(t, err)

	_, err = ParseInputValue(map[string]interface{}{
		"end": endStr,
	}, cwl.DateRangeType{})
	assert.NotNil(t, err)
}

func TestParseBoundingBox(t *testing.T) {
	lat1 := 43.1
	lat2 := 43.2
	lng1 := 3.1
	lng2 := 3.2

	fields := make(map[string]interface{})
	fields["lat1"] = lat1
	fields["lat2"] = lat2
	fields["lng1"] = lng1
	fields["lng2"] = lng2

	value, err := ParseInputValue(fields, cwl.BoundingBoxType{})

	assert.Nil(t, err)
	assert.IsType(t, BoundingBoxValue{}, value)
	assert.IsType(t, BoundingBox{}, value.GetValue())
	assert.Equal(t, lat1, value.GetValue().(BoundingBox).Lat1)
	assert.Equal(t, lat2, value.GetValue().(BoundingBox).Lat2)
	assert.Equal(t, lng1, value.GetValue().(BoundingBox).Lng1)
	assert.Equal(t, lng2, value.GetValue().(BoundingBox).Lng2)
}
