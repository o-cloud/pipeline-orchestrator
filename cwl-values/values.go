package cwl_values

import (
	"fmt"
	"gitlab.com/o-cloud/cwl"
	"strconv"
	"strings"
	"time"
)

type Value interface {
	IsValue()
	GetValue() interface{}
}

type IntValue struct {
	Value int
}

type BooleanValue struct {
	Value bool
}

type LongValue struct {
	Value int64
}

type FloatValue struct {
	Value float32
}

type DoubleValue struct {
	Value float64
}

type StringValue struct {
	Value string
}

type FileValue struct {
	Path string
}

type ArrayValue struct {
	Value []Value
}

type EnumValue struct {
	Value string
}

type RecordValue struct {
	Value map[string]Value
}

type DateRange struct {
	Start string `json:"start"`
	End   string `json:"end"`
}
type DateRangeValue struct {
	Value DateRange
}

type BoundingBox struct {
	Lat1 float64 `json:"lat1"`
	Lat2 float64 `json:"lat2"`
	Lng1 float64 `json:"lng1"`
	Lng2 float64 `json:"lng2"`
}

type BoundingBoxValue struct {
	Value BoundingBox
}

func (b BoundingBoxValue) IsValue() {}
func (b BoundingBoxValue) GetValue() interface{} {
	return b.Value
}
func (b BoundingBoxValue) Parse(value interface{}) (Value, error) {
	v, ok := value.(map[string]interface{})
	if !ok {
		return nil, fmt.Errorf("expected map[string]interface{} with fields 'lat1', 'lng1', 'lat2', 'lng2', found %T", value)
	}
	lat1I, ok := v["lat1"]
	if !ok {
		return nil, fmt.Errorf("expected field 'lat1' doesn't exists")
	}
	lat2I, ok := v["lat2"]
	if !ok {
		return nil, fmt.Errorf("expected field 'lat2' doesn't exists")
	}
	lng1I, ok := v["lng1"]
	if !ok {
		return nil, fmt.Errorf("expected field 'lng1' doesn't exists")
	}
	lng2I, ok := v["lng2"]
	if !ok {
		return nil, fmt.Errorf("expected field 'lng2' doesn't exists")
	}
	parseFloat := func(o interface{}, fieldName string) (float64, error) {
		res, ok := o.(float64)
		if !ok {
			str, ok := o.(string)
			if !ok {
				return 0, fmt.Errorf("cannot parse field %s expected float64 found %T", fieldName, o)
			}
			res, err := strconv.ParseFloat(str, 64)
			if err != nil {
				return 0, fmt.Errorf("cannot parse field %s expected type float64 found %s, %s", fieldName, str, err)
			}
			return res, nil
		}
		return res, nil
	}

	lat1, err := parseFloat(lat1I, "lat1")
	if err != nil {
		return nil, err
	}
	lat2, err := parseFloat(lat2I, "lat2")
	if err != nil {
		return nil, err
	}
	lng1, err := parseFloat(lng1I, "lng1")
	if err != nil {
		return nil, err
	}
	lng2, err := parseFloat(lng2I, "lng2")
	if err != nil {
		return nil, err
	}
	return BoundingBoxValue{
		Value: BoundingBox{
			Lat1: lat1,
			Lat2: lat2,
			Lng1: lng1,
			Lng2: lng2,
		},
	}, nil
}

func (d DateRangeValue) IsValue() {}
func (d DateRangeValue) Parse(value interface{}) (Value, error) {
	v, ok := value.(map[string]interface{})
	if !ok {
		return nil, fmt.Errorf("expected map[string]interface{} with fields start and end, found %T", value)
	}
	startI, ok := v["start"]
	if !ok {
		return nil, fmt.Errorf("expected field 'start' doesn't exists")
	}
	endI, ok := v["end"]
	if !ok {
		return nil, fmt.Errorf("expected field 'end' doesn't exists")
	}
	startStr, ok := startI.(string)
	if !ok {
		return nil, fmt.Errorf("expected field 'start' to have a string value, found a %T", startI)
	}

	endStr, ok := endI.(string)
	if !ok {
		return nil, fmt.Errorf("expected field 'end' to have a string value, found a %T", endI)
	}

	_, err := time.Parse(time.RFC3339, startStr)
	if err != nil {
		return nil, fmt.Errorf("cannot parse 'start', value %s is not valid according to RFC3339", startI)
	}
	_, err = time.Parse(time.RFC3339, endStr)
	if err != nil {
		return nil, fmt.Errorf("cannot parse 'end', value %s is not valid according to RFC3339", endI)
	}
	result := DateRangeValue{
		Value: DateRange{
			Start: startStr,
			End:   endStr,
		},
	}
	return result, nil
}

func (d DateRangeValue) GetValue() interface{} {
	return d.Value
}

func (i IntValue) IsValue() {}

func (i IntValue) Parse(value interface{}) (Value, error) {
	v, ok := value.(int)
	if !ok {
		v, ok := value.(string)
		if !ok {
			return nil, fmt.Errorf("expected int found %T", value)
		}
		i, err := strconv.ParseInt(v, 10, 32)
		if err != nil {
			return nil, fmt.Errorf("expected int found %T", value)
		}
		return IntValue{Value: int(i)}, nil
	}
	return IntValue{Value: v}, nil
}

func (i IntValue) GetValue() interface{} {
	return i.Value
}

func (i BooleanValue) IsValue() {}

func (i BooleanValue) Parse(value interface{}) (Value, error) {
	v, ok := value.(bool)
	if !ok {
		return nil, fmt.Errorf("expected boolean found %T", value)
	}
	return BooleanValue{Value: v}, nil
}

func (i BooleanValue) GetValue() interface{} {
	return i.Value
}

func (i FloatValue) IsValue() {}

func (i FloatValue) Parse(value interface{}) (Value, error) {
	v, ok := value.(float32)
	if ok {
		return FloatValue{Value: v}, nil
	}

	v64, ok := value.(float64)
	if ok {
		return FloatValue{Value: float32(v64)}, nil
	}

	vInt, ok := value.(int)
	if ok {
		return FloatValue{Value: float32(vInt)}, nil
	} else {
		v, ok := value.(string)
		if !ok {
			return nil, fmt.Errorf("expected float found %T", value)
		}
		float, err := strconv.ParseFloat(v, 32)
		if err != nil {
			return nil, fmt.Errorf("expected float found %T", value)
		}
		return FloatValue{Value: float32(float)}, nil
	}
}

func (i FloatValue) GetValue() interface{} {
	return i.Value
}

func (i LongValue) IsValue() {}

func (i LongValue) Parse(value interface{}) (Value, error) {
	v, ok := value.(int64)
	if ok {
		return LongValue{Value: v}, nil
	}

	v32, ok := value.(int32)
	if ok {
		return LongValue{Value: int64(v32)}, nil
	}

	vInt, ok := value.(int)
	if ok {
		return LongValue{Value: int64(vInt)}, nil
	}

	str, ok := value.(string)
	if ok {
		i, err := strconv.ParseInt(str, 10, 64)
		if err != nil {
			return nil, fmt.Errorf("expected long found %T", value)
		}
		return LongValue{Value: i}, nil
	}
	return nil, fmt.Errorf("expected long found %T", value)
}

func (i LongValue) GetValue() interface{} {
	return i.Value
}

func (i DoubleValue) IsValue() {}

func (i DoubleValue) Parse(value interface{}) (Value, error) {
	v, ok := value.(float32)
	if ok {
		return DoubleValue{Value: float64(v)}, nil
	}

	v64, ok := value.(float64)
	if ok {
		return DoubleValue{Value: v64}, nil
	}

	vInt, ok := value.(int)
	if ok {
		return DoubleValue{Value: float64(vInt)}, nil
	} else {
		v, ok := value.(string)
		if !ok {
			return nil, fmt.Errorf("expected double found %T", value)
		}
		float, err := strconv.ParseFloat(v, 64)
		if err != nil {
			return nil, fmt.Errorf("expected double found %T", value)
		}
		return DoubleValue{Value: float}, nil
	}
}

func (i DoubleValue) GetValue() interface{} {
	return i.Value
}

func (s StringValue) IsValue() {}

func (s StringValue) Parse(value interface{}) (Value, error) {
	v, ok := value.(string)
	if !ok {
		return nil, fmt.Errorf("expected string found %T", value)
	}
	return StringValue{Value: v}, nil
}

func (s StringValue) GetValue() interface{} {
	return s.Value
}

func (s FileValue) IsValue() {}

func (s FileValue) Parse(value interface{}) (Value, error) {
	panic("not implemented")
}

func (s FileValue) GetValue() interface{} {
	return s.Path
}

func (a ArrayValue) IsValue() {}

func (a ArrayValue) Parse(value interface{}, itemsType cwl.CommandInputType) (Value, error) {
	array, ok := value.([]interface{})
	if !ok {
		return nil, fmt.Errorf("expected array found %T", value)
	}
	result := make([]Value, len(array))
	for i, item := range array {
		v, err := ParseInputValue(item.(map[string]interface{})["value"], itemsType)
		if err != nil {
			return nil, fmt.Errorf("cannot parse array item %d, %s", i, err)
		}
		result[i] = v
	}
	return ArrayValue{Value: result}, nil
}

func (a ArrayValue) GetValue() interface{} {
	r := make([]interface{}, len(a.Value))
	for i, value := range a.Value {
		r[i] = value.GetValue()
	}
	return r
}

func (e EnumValue) IsValue() {}

func (e EnumValue) Parse(value interface{}, symbols []string) (Value, error) {
	str, ok := value.(string)
	if !ok {
		return nil, fmt.Errorf("expected string for enum found %T", value)
	}
	for _, symbol := range symbols {
		if symbol == str {
			return EnumValue{Value: symbol}, nil
		}
	}
	return nil, fmt.Errorf("cannot parse enum, %s is not in enum symbols %s", str, strings.Join(symbols, ", "))
}

func (e EnumValue) GetValue() interface{} {
	return e.Value
}

func (r RecordValue) IsValue() {}

func (r RecordValue) Parse(value interface{}, fields map[string]cwl.CommandInputRecordField) (Value, error) {
	m, ok := value.(map[string]interface{})
	if !ok {
		return nil, fmt.Errorf("cannot parse record expected map[string]interface{} found %T", value)
	}
	result := RecordValue{Value: make(map[string]Value, len(m))}
	for key, t := range fields {
		toParse, ok := m[key]
		if !ok {
			return nil, fmt.Errorf("cannot parse record, expected field %s does not exists in %+v", key, m)
		}
		inputValue, err := ParseInputValue(toParse, t.Type.Wrapped)
		if err != nil {
			return nil, fmt.Errorf("cannot parse record, cannot parse field %s, %s", key, err)
		}
		result.Value[key] = inputValue
	}
	return result, nil
}

func (r RecordValue) GetValue() interface{} {
	m := make(map[string]interface{}, len(r.Value))
	for key, value := range r.Value {
		m[key] = value.GetValue()
	}
	return m
}

func ParseInputValue(value interface{}, inputType cwl.CommandInputType) (Value, error) {
	switch inputType.(type) {
	case cwl.DirectoryType:
		return nil, fmt.Errorf("type directory is not handled yet")
	case cwl.FileType:
		return nil, fmt.Errorf("type file is not handled yet")
	case cwl.StringType:
		return StringValue{}.Parse(value)
	case cwl.DoubleType:
		return DoubleValue{}.Parse(value)
	case cwl.FloatType:
		return FloatValue{}.Parse(value)
	case cwl.IntegerType:
		return IntValue{}.Parse(value)
	case cwl.LongType:
		return LongValue{}.Parse(value)
	case cwl.BooleanType:
		return BooleanValue{}.Parse(value)
	case cwl.DateRangeType:
		return DateRangeValue{}.Parse(value)
	case cwl.BoundingBoxType:
		return BoundingBoxValue{}.Parse(value)
	case cwl.CommandInputArraySchema:
		return ArrayValue{}.Parse(value, inputType.(cwl.CommandInputArraySchema).Items.Wrapped)
	case cwl.CommandInputRecordSchema:
		return RecordValue{}.Parse(value, inputType.(cwl.CommandInputRecordSchema).Fields)
	case cwl.CommandInputUnionSchema:
		for _, t := range inputType.(cwl.CommandInputUnionSchema).Types {
			result, err := ParseInputValue(value, t.Wrapped)
			if err == nil {
				return result, nil
			}
		}
		return nil, fmt.Errorf("cannot parse union, no type matched value %+v", value)
	case cwl.CommandInputEnumSchema:
		return EnumValue{}.Parse(value, inputType.(cwl.CommandInputEnumSchema).Symbols)
	default:
		return nil, fmt.Errorf("unhandled type %T", inputType)
	}
}
