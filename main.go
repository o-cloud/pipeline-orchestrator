package main

import (
	"strings"

	nestedLog "github.com/antonfisher/nested-logrus-formatter"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/o-cloud/pipeline-orchestrator/api"
	"gitlab.com/o-cloud/pipeline-orchestrator/config"
)

func main() {
	config.Load()
	initLogger()
	r := setupRouter()

	if err := r.Run(config.Config.Server.ListenAddress); err != nil {
		log.Println(err)
	}
}

func setupRouter() *gin.Engine {
	r := gin.Default()
	api.NewHandler().SetUpRoutes(r.Group(config.Config.Server.ApiPrefix + "/executions"))
	return r
}

func initLogger() {
	log.SetReportCaller(true)
	log.SetFormatter(&nestedLog.Formatter{
		HideKeys: true,
	})
	logLevelStr := strings.TrimSpace(viper.GetString("LOG_LEVEL"))
	logLevel, err := log.ParseLevel(logLevelStr)
	if err != nil {
		log.SetLevel(log.InfoLevel)
		log.Warnf("log level %s is not valid", logLevelStr)
	} else {
		log.SetLevel(logLevel)
		log.Infof("log level set to %s", logLevelStr)
	}
}
