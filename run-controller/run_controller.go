package run_controller

import (
	"fmt"
	"log"
	"regexp"
	"strconv"
	"strings"

	"github.com/argoproj/argo-workflows/v3/pkg/apis/workflow/v1alpha1"
	"github.com/google/uuid"
	argo_client "gitlab.com/o-cloud/pipeline-orchestrator/argo-client"
	compute_provider_client "gitlab.com/o-cloud/pipeline-orchestrator/compute-provider-client"
	kubernetes_client "gitlab.com/o-cloud/pipeline-orchestrator/kubernetes-client"
	"gitlab.com/o-cloud/pipeline-orchestrator/models"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const nodeSelectorKey = "irtsb.cluster"

type RunConfig struct {
	TargetNamespace string
}

func Run(workflow *v1alpha1.Workflow, config RunConfig, request models.StartWorkflowRequest) (*v1alpha1.Workflow, error) {
	identity := getIdentityString()
	fmt.Println("Prepare local cluster")
	err := compute_provider_client.PrepareLocalClusterBeforeExecution(config.TargetNamespace)
	if err != nil {
		return nil, err
	}

	fmt.Println("Add anotation")
	err = addAnnotations(workflow, identity, config, request)
	if err != nil {
		return nil, err
	}

	fmt.Println("Add s3 bucket")
	err = addS3Bucket(workflow, config, request)
	if err != nil {
		return nil, err
	}

	fmt.Println("start workflow")
	argoClient := argo_client.NewClient()
	wf, err := argoClient.StartWorkflow(*workflow, config.TargetNamespace)
	if err != nil {
		return nil, fmt.Errorf("error in argo client, %s", err)
	}

	return wf, nil
}

func getIdentityString() string {
	return "todo-cluster-id"
}

type s3Data struct {
	models.S3Credentials
	endpoint string
}

func addS3Bucket(workflow *v1alpha1.Workflow, config RunConfig, request models.StartWorkflowRequest) error {
	buildSelector := func(blockId int, selector string) string { return strconv.Itoa(blockId) + "-" + selector }
	accessKeySelector := "accessKey"
	secretKeySelector := "secretKey"
	bucketName := "bucket1"
	s3Port := "9001"
	credByBlockId := make(map[int]s3Data)
	for _, block := range request.Blocks {
		log.Println("for block", block.Name)
		cred := s3Data{
			S3Credentials: block.Payload.S3Provider.Metadata.S3,
			endpoint:      extractIP(block.Payload.S3Provider.EndpointLocation) + ":" + s3Port,
		}
		credByBlockId[block.Index] = cred
	}
	secretData := make(map[string][]byte)
	for key, value := range credByBlockId {
		secretData[buildSelector(key, accessKeySelector)] = []byte(value.AccessKey)
		secretData[buildSelector(key, secretKeySelector)] = []byte(value.SecretKey)
	}
	id := uuid.NewString()
	secretName := fmt.Sprintf("wf-%s-s3-credentials", id)
	secret := corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name: secretName,
		},
		Data: secretData,
	}
	err := kubernetes_client.CreateSecret(config.TargetNamespace, secret)
	if err != nil {
		return err
	}
	insecure := true

	for i := range workflow.Spec.Templates {
		if workflow.Spec.Templates[i].DAG != nil {
			continue //Skip DAG template
		}
		if len(workflow.Spec.Templates[i].Outputs.Artifacts) == 0 {
			continue //Skip if no output
		}
		blockIndex, err := strconv.Atoi(workflow.Spec.Templates[i].Metadata.Annotations["blockIndex"])
		if err != nil {
			return fmt.Errorf("error while parsing block index, %s", err)
		}
		cred, ok := credByBlockId[blockIndex]
		if !ok {
			return fmt.Errorf("cannot find s3 credentials for block with id %d", blockIndex)
		}
		bucket := v1alpha1.S3Bucket{
			Endpoint: cred.endpoint,
			Bucket:   bucketName,
			Insecure: &insecure,
			AccessKeySecret: &corev1.SecretKeySelector{
				LocalObjectReference: corev1.LocalObjectReference{
					Name: secretName,
				},
				Key: buildSelector(blockIndex, accessKeySelector),
			},
			SecretKeySecret: &corev1.SecretKeySelector{
				LocalObjectReference: corev1.LocalObjectReference{
					Name: secretName,
				},
				Key: buildSelector(blockIndex, secretKeySelector),
			},
			RoleARN:                  "",
			UseSDKCreds:              false,
			CreateBucketIfNotPresent: nil,
		}
		for j := range workflow.Spec.Templates[i].Outputs.Artifacts {
			workflow.Spec.Templates[i].Outputs.Artifacts[j].S3 = &v1alpha1.S3Artifact{
				S3Bucket: bucket,
				Key:      fmt.Sprintf("wf-%s/%s/%s-{{pod.name}}", id, workflow.Spec.Templates[i].Name, workflow.Spec.Templates[i].Outputs.Artifacts[j].Name),
			}
		}
	}
	return nil
}

//func addBuckets(workflow *v1alpha1.Workflow, config RunConfig) error {
//
//	id := uuid.NewString()
//	secretName := fmt.Sprintf("wf-%s-gcp-credentials", id)
//	secretKey := "serviceAccountKey"
//	decodedToken, _ := base64.StdEncoding.DecodeString(config.GcpBucketToken)
//	secret := corev1.Secret{
//		ObjectMeta: metav1.ObjectMeta{
//			Name: secretName,
//		},
//		Data: map[string][]byte{
//			secretKey: decodedToken,
//		},
//	}
//	err := kubernetes_client.CreateSecret(config.TargetNamespace, secret)
//	if err != nil {
//		return err
//	}
//	bucket := v1alpha1.GCSBucket{
//		Bucket: config.GcpBucketName,
//		ServiceAccountKeySecret: &corev1.SecretKeySelector{
//			LocalObjectReference: corev1.LocalObjectReference{
//				Name: secretName,
//			},
//			Key: secretKey,
//		},
//	}
//	for i := range workflow.Spec.Templates {
//		for j := range workflow.Spec.Templates[i].Outputs.Artifacts {
//			workflow.Spec.Templates[i].Outputs.Artifacts[j].GCS = &v1alpha1.GCSArtifact{
//				GCSBucket: bucket,
//				Key:       fmt.Sprintf("wf-%s/%s/%s", id, workflow.Spec.Templates[i].Name, workflow.Spec.Templates[i].Outputs.Artifacts[j].Name),
//			}
//		}
//	}
//	return nil
//}

func extractIP(url string) string {
	r := regexp.MustCompile("https?://([^/]+)")
	result := r.FindStringSubmatch(url)
	if len(result) < 2 {
		log.Println("extracct ip result error ", url, result)
	}
	return result[1]
}

func remove443PortFromIP(ip string) string {
	if ip[len(ip)-4:] == ":443" {
		ip = ip[:len(ip)-4]
	}
	return ip
}

func sanitizeStringForKubernetes(myString string) string {
	// Remove forbiden characters
	myString = strings.Replace(myString, "_", "-", -1)
	myString = strings.Replace(myString, "https://", "", -1)
	myString = strings.Replace(myString, ":", "-", -1)
	myString = strings.Replace(myString, ".", "-", -1)

	//	The release name has a limited size
	if len(myString) > 52 {
		myString = string(myString[:52])
	}
	return myString
}

func addAnnotations(workflow *v1alpha1.Workflow, identity string, runConfig RunConfig, request models.StartWorkflowRequest) error {
	cpMap := map[string]models.ComputeProvider{}
	for i := range workflow.Spec.Templates {
		t := &workflow.Spec.Templates[i]
		if t.DAG != nil {
			continue //Skip DAG template
		}
		blockIndex, err := strconv.Atoi(t.Metadata.Annotations["blockIndex"])
		if err != nil {
			return fmt.Errorf("error while parsing block index, %s", err)
		}
		for _, block := range request.Blocks {
			if block.Index == blockIndex {
				cp := block.Payload.ComputeProvider
				if cp.IsLocal {
					break
				}
				if _, exists := cpMap[cp.EndpointLocation]; !exists {
					cpMap[cp.EndpointLocation] = cp
				}
				t.Metadata.Annotations["multicluster.admiralty.io/elect"] = ""
				if t.NodeSelector == nil {
					t.NodeSelector = map[string]string{}
				}
				ip := extractIP(cp.Metadata.KubernetesApi)
				ip = remove443PortFromIP(ip)
				t.NodeSelector[nodeSelectorKey] = sanitizeStringForKubernetes(ip)
			}
		}
		t.Metadata.Labels["kubecost"] = identity
		t.Metadata.Labels["workflow"] = request.ID
	}
	cpList := make([]models.ComputeProvider, 0, len(cpMap))
	for _, cp := range cpMap {
		cpList = append(cpList, cp)
	}
	compute_provider_client.Book(cpList, runConfig.TargetNamespace, identity)
	return nil
}
