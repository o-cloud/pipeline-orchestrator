[![pipeline status](https://gitlab.com/o-cloud/pipeline-orchestrator/badges/master/pipeline.svg)](https://gitlab.com/o-cloud/pipeline-orchestrator/)
[![coverage report](https://gitlab.com/o-cloud/pipeline-orchestrator/badges/master/coverage.svg)](https://gitlab.com/o-cloud/pipeline-orchestrator/)

# Pipeline Orchestrator

## Overview

The pipeline orchestrator is responsible for:

* parsing a workflow and generating an executable Argo Workflow
* contacting the data providers involved in the workflow
* contacting the compute provider in order to ensure that the local cluster is ready to run the workflow, and is
  connected to the remote clusters involved in the execution
* starting the execution of the Argo Workflow

When a [StartWorkflowRequest](models/api_models.go) is received, the body of the request will first be translated into a
graph model (see [ToGraph](models/api_models.go)).

The resulting graph will then be translated into an Argo Workflow see [graph](graph) package. The cwl of each task and
the parameters supplied are used to create Argo Templates and containers. Between each Argo Containers, the input and
outputs are passed using a S3 container as defined in the StartWorkflowRequest (
see [addS3Bucket](run-controller/run_controller.go) function). At the end we have an executable Argo Workflow.

Admiralty specific annotations are added to this workflow if multi cluster execution is needed (
see [addAnnotation](run-controller/run_controller.go) function in run controller)

The compute provider is called to ensure that our local cluster is ready to execute the workflow, and that it is
well-connected to all the others clusters involved in the execution (
see [compute_provider_client.go](compute-provider-client/compute_provider_client.go))

Starting the workflow is handled by [argo_client.go](argo-client/argo_client.go)

## Prerequisites

- `helm`
- `skaffold`
- `kubectl`

> You'll need an active kubectl context to use this API in skaffold

## Usage

### Run

```bash
skaffold run
```

Option *default-repo* need to be added in skaffold command for specify container registry to used.

### Dev

```bash
skaffold dev
```

## Test

From the project root, run :

```bash
go test ./... -cover
```

## Configuration

### Server

| Name          | Type   | Default                       |
|---------------|--------|-------------------------------|
| listenAddress | string | ":9092"                       |
| apiPrefix     | string | "/api/pipeline-orchestrator"  |

### Environment variables

| Name                 | Type   | Default                                |
|----------------------|--------|----------------------------------------|
| MONGO_DATABASE       | string | "service-provider"                     |
| COMPUTE_PROVIDER_API | string | "http://compute-provider-service:8080" |
| LOG_LEVEL            | string | "INFO"                                 |

# Api

## Execute workflow

**URL**: `/api/pipeline-orchestrator/start`

**METHOD**: `POST`

**Path Params**: None

**Query Params**: None

**Request Body**: [StartWorkflowRequest](models/api_models.go)

### Success Response

**Code**: `200 OK`

**Content**: [StartWorkflowResponse](models/api_models.go)

### Error Response

**Code**: `500 Internal Server Error`

**Or**

**Code**: `422 Unprocessable entity`
the request body cannot be parsed

**Or**

**Code**: `400 Bad Request`
the request body has been parsed but is invalid
