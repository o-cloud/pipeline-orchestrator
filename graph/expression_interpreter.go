package graph

import (
	"fmt"
	values "gitlab.com/o-cloud/pipeline-orchestrator/cwl-values"
	"strings"
)

type ExpressionInterpreter struct {
	inputsValues map[string]values.Value
}

func NewExpressionInterpreter(inputsValues map[string]values.Value) *ExpressionInterpreter {
	e := ExpressionInterpreter{inputsValues: inputsValues}
	return &e
}

func IsExpression(str string) bool {
	return strings.HasPrefix(str, "$(") && strings.HasSuffix(str, ")")
}

func cleanExpression(str string) string {
	str = strings.TrimPrefix(str, "$(")
	str = strings.TrimSuffix(str, ")")
	str = strings.TrimSpace(str)
	return str
}

func (e *ExpressionInterpreter) Interpret(expression string) (values.Value, error) {
	if !IsExpression(expression) {
		return nil, fmt.Errorf("string %s is not an expression", expression)
	}
	expression = cleanExpression(expression)
	parts := strings.Split(expression, ".")
	//TODO we need to improve this part and find a way to handle the potential complexity of interpreting expressions
	if len(parts) == 2 && parts[0] == "inputs" {
		value := e.inputsValues[parts[1]]
		if value == nil {
			return nil, fmt.Errorf("failed to interpret expression %s cannot find %s in inputs", expression, parts[1])
		}
		return value, nil
	}
	return nil, fmt.Errorf("failed to interpret expression %s", expression)
}
