package graph

import (
	"errors"
	"gitlab.com/o-cloud/cwl"
	values "gitlab.com/o-cloud/pipeline-orchestrator/cwl-values"
	"strconv"
	"strings"
)

type Node struct {
	BlockIndex       int
	Cwl              cwl.CommandLineTool
	Inputs           map[string]values.Value
	name             string
	ProviderType     string
	EndpointLocation string
}

type Link struct {
	From     *Node
	To       *Node
	OutputID string
	InputID  string
}

type Graph struct {
	nodes []*Node
	links Links
}

type Links []*Link

func (links *Links) filter(filter func(*Link) bool) Links {
	var filtered Links
	for _, link := range *links {
		if filter(link) {
			filtered = append(filtered, link)
		}
	}
	return filtered
}

func (links *Links) toOriginsName() []string {
	namesMap := make(map[string]interface{})
	for _, link := range *links {
		namesMap[link.From.name] = struct{}{}
	}
	i := 0
	names := make([]string, len(namesMap))
	for originName := range namesMap {
		names[i] = originName
		i++
	}
	return names
}

func (g *Graph) nameFields() {
	for i, node := range g.nodes {
		name := node.Cwl.Label
		name = strings.Trim(name, "")
		name = strings.ReplaceAll(name, " ", "-")
		g.nodes[i].name = name + "-" + toThreeDigits(i)
	}
}

func toThreeDigits(i int) string {
	s := strconv.Itoa(i)
	for len(s) < 3 {
		s = "0" + s
	}
	return s
}

func NewGraph(nodes []*Node, links []*Link) (*Graph, error) {
	g := Graph{
		nodes: nodes,
		links: links,
	}
	err := sortNodes(&g)
	if err != nil {
		return nil, err
	}
	g.nameFields()
	return &g, nil
}

func sortNodes(graph *Graph) error {
	var queue []*Node
	var sorted []*Node
	inDegrees := make(map[*Node]int, len(graph.nodes))
	for _, node := range graph.nodes {
		inDegrees[node] = len(findIncomingLinks(graph, node))
		if inDegrees[node] == 0 {
			queue = append(queue, node)
		}
	}
	for len(queue) > 0 {
		e := queue[0]
		queue = queue[1:]
		sorted = append(sorted, e)
		for _, child := range findChildren(graph, e) {
			inDegrees[child] -= 1
			if inDegrees[child] == 0 {
				queue = append(queue, child)
			}
		}
	}
	for _, inDegree := range inDegrees {
		if inDegree > 0 {
			return errors.New("graph is not valid")
		}
	}
	graph.nodes = sorted
	return nil
}

func findChildren(graph *Graph, node *Node) []*Node {
	var nodes []*Node
	for _, link := range graph.links {
		if link.From == node {
			nodes = append(nodes, link.To)
		}
	}
	return nodes
}

func outputIsLinked(graph *Graph, node *Node, outputID string) bool {
	for _, link := range graph.links {
		if link.From == node && link.OutputID == outputID {
			return true
		}
	}
	return false
}

func inputIsLinked(graph *Graph, node *Node, inputID string) bool {
	for _, link := range graph.links {
		if link.To == node && link.InputID == inputID {
			return true
		}
	}
	return false
}

func findIncomingLinks(graph *Graph, node *Node) Links {
	return graph.links.filter(func(l *Link) bool { return l.To == node })
}
