package graph

import (
	"github.com/argoproj/argo-workflows/v3/pkg/apis/workflow/v1alpha1"
	"github.com/stretchr/testify/assert"
	"gitlab.com/o-cloud/cwl"
	values "gitlab.com/o-cloud/pipeline-orchestrator/cwl-values"
	v1c "k8s.io/api/core/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"testing"
)

type mockDataProviderClient struct {
}

func (c *mockDataProviderClient) ReserveData(url string, inputs map[string]values.Value) (map[string]values.Value, error) {
	return map[string]values.Value{
		"url": values.StringValue{Value: "http://data-provider/image"},
	}, nil
}

func TestBuilder(t *testing.T) {
	g := Graph{
		nodes: []*Node{
			&Node{
				Cwl: cwl.CommandLineTool{
					Label:       "Sentinel",
					Class:       "CommandLineTool",
					BaseCommand: []string{"wget"},
					Arguments: []cwl.CommandLineBinding{
						{
							IsFilled:   true,
							Position:   0,
							Separate:   true,
							ValueFrom:  "--tries=1",
							ShellQuote: true,
						},
						{
							IsFilled:   true,
							Position:   0,
							Separate:   true,
							ValueFrom:  "-O",
							ShellQuote: true,
						},
						{
							IsFilled:   true,
							Position:   0,
							Separate:   true,
							ValueFrom:  "image.tif",
							ShellQuote: true,
						},
					},
					Inputs: cwl.Inputs{
						"boundingBox": cwl.CommandInputParameter{
							InputBinding: cwl.CommandLineBinding{
								IsFilled:      false,
								Position:      0,
								Prefix:        "",
								Separate:      false,
								ItemSeparator: "",
								ValueFrom:     "",
								ShellQuote:    false,
							},
							Type: cwl.CommandInputTypeWrapper{
								Wrapped: cwl.CommandInputArraySchema{
									Items: cwl.CommandInputTypeWrapper{
										Wrapped: cwl.FloatType{
											Type: "float",
										},
									},
									InputBinding: cwl.CommandLineBinding{
										IsFilled:      false,
										Position:      0,
										Prefix:        "",
										Separate:      false,
										ItemSeparator: "",
										ValueFrom:     "",
										ShellQuote:    false,
									},
								},
							},
							InputType: "definition",
						},
						"fromDate": cwl.CommandInputParameter{
							InputBinding: cwl.CommandLineBinding{
								IsFilled:      false,
								Position:      0,
								Prefix:        "",
								Separate:      false,
								ItemSeparator: "",
								ValueFrom:     "",
								ShellQuote:    false,
							},
							Type: cwl.CommandInputTypeWrapper{
								Wrapped: cwl.StringType{},
							},
							InputType: "execution",
						},
						"toDate": cwl.CommandInputParameter{
							InputBinding: cwl.CommandLineBinding{
								IsFilled:      false,
								Position:      0,
								Prefix:        "",
								Separate:      false,
								ItemSeparator: "",
								ValueFrom:     "",
								ShellQuote:    false,
							},
							Type: cwl.CommandInputTypeWrapper{
								Wrapped: cwl.StringType{},
							},
							InputType: "execution",
						},
						"url": cwl.CommandInputParameter{
							InputBinding: cwl.CommandLineBinding{
								IsFilled:      true,
								Position:      1,
								Prefix:        "",
								Separate:      true,
								ItemSeparator: "",
								ValueFrom:     "",
								ShellQuote:    true,
							},
							Type: cwl.CommandInputTypeWrapper{
								Wrapped: cwl.StringType{},
							},
							InputType: "orchestrator",
						},
					},
					Outputs: cwl.Outputs{
						"downloadedFile": cwl.CommandOutputParameter{
							Binding: cwl.CommandOutputBinding{
								Glob:       []string{"image.tif"},
								OutputEval: "",
							},
							Type: cwl.CommandOutputTypeWrapper{
								Wrapped: cwl.FileType{},
							},
						},
					},
					Requirements: cwl.Requirements{cwl.DockerRequirement{
						DockerPull: "mwendler/wget:latest",
					}},
				},
				Inputs: map[string]values.Value{
					"boundingBox": values.ArrayValue{Value: []values.Value{
						values.FloatValue{Value: 3.1},
						values.FloatValue{Value: 43.1},
						values.FloatValue{Value: 3.2},
						values.FloatValue{Value: 43.2}},
					},
					"fromDate": values.StringValue{Value: "2020-08-22T00:00:00Z"},
					"toDate":   values.StringValue{Value: "2020-09-22T00:00:00Z"},
				},
				name:             "Sentinel-000",
				ProviderType:     "data",
				EndpointLocation: "http://localhost:9093/api/provider/data/sentinel",
				BlockIndex:       0,
			},
			&Node{
				Cwl: cwl.CommandLineTool{
					Label:       "Converts an image",
					Class:       "CommandLineTool",
					BaseCommand: []string{"gdal_translate"},
					Arguments: []cwl.CommandLineBinding{
						{
							IsFilled:      true,
							Position:      0,
							Prefix:        "",
							Separate:      true,
							ItemSeparator: "",
							ValueFrom:     "-of",
							ShellQuote:    true,
						},
					},
					Inputs: cwl.Inputs{
						"inputFile": cwl.CommandInputParameter{
							InputBinding: cwl.CommandLineBinding{
								IsFilled:      true,
								Position:      1,
								Prefix:        "",
								Separate:      true,
								ItemSeparator: "",
								ValueFrom:     "",
								ShellQuote:    true,
							},
							Type: cwl.CommandInputTypeWrapper{
								Wrapped: cwl.FileType{},
							},
							InputType: "pipeline",
						},
						"outputFormat": cwl.CommandInputParameter{
							InputBinding: cwl.CommandLineBinding{
								IsFilled:      true,
								Position:      0,
								Prefix:        "",
								Separate:      true,
								ItemSeparator: "",
								ValueFrom:     "",
								ShellQuote:    true,
							},
							Type: cwl.CommandInputTypeWrapper{
								Wrapped: cwl.StringType{},
							},
							InputType: "definition",
						},
						"outputName": cwl.CommandInputParameter{
							InputBinding: cwl.CommandLineBinding{
								IsFilled:      true,
								Position:      2,
								Prefix:        "",
								Separate:      true,
								ItemSeparator: "",
								ValueFrom:     "",
								ShellQuote:    true,
							},
							Type:      cwl.CommandInputTypeWrapper{Wrapped: cwl.StringType{}},
							InputType: "definition",
						},
					},
					Outputs: cwl.Outputs{
						"convertedFile": cwl.CommandOutputParameter{
							Binding: cwl.CommandOutputBinding{
								Glob:       []string{"$(inputs.outputName)"},
								OutputEval: "",
							},
							Type: cwl.CommandOutputTypeWrapper{
								Wrapped: cwl.FileType{},
							},
						},
					},
					Requirements: cwl.Requirements{
						cwl.DockerRequirement{
							Type:       "DockerRequirement",
							DockerPull: "geodata/gdal",
						},
					},
				},
				Inputs: map[string]values.Value{
					"outputFormat": values.StringValue{Value: "PNG"},
					"outputName":   values.StringValue{Value: "image.png"},
				},
				name:             "Converts-an-image-001",
				ProviderType:     "service",
				EndpointLocation: "http://localhost",
				BlockIndex:       1,
			},
			&Node{
				Cwl: cwl.CommandLineTool{
					Label:       "Push To S3",
					Class:       "CommandLineTool",
					BaseCommand: []string{"command"},
					Arguments:   nil,
					Inputs: cwl.Inputs{
						"fileIn": cwl.CommandInputParameter{
							InputBinding: cwl.CommandLineBinding{
								IsFilled:   true,
								Position:   0,
								Separate:   true,
								ShellQuote: true,
							},
							Type: cwl.CommandInputTypeWrapper{
								Wrapped: cwl.FileType{},
							},
							InputType: "pipeline",
						},
						"fileName": cwl.CommandInputParameter{
							InputBinding: cwl.CommandLineBinding{
								IsFilled:   true,
								Position:   1,
								Prefix:     "gs://workflow-output/$(date +%s-%N)",
								ShellQuote: true,
							},
							Type:      cwl.CommandInputTypeWrapper{Wrapped: cwl.StringType{}},
							InputType: "definition",
						},
					},
					Outputs: nil,
					Requirements: cwl.Requirements{
						cwl.DockerRequirement{
							Type:       "DockerRequirement",
							DockerPull: "gcr.io/google.com/cloudsdktool/cloud-sdk:alpine",
						},
					},
				},
				Inputs: map[string]values.Value{
					"fileName": values.StringValue{Value: "image.png"},
				},
				name:             "Push-To-S3-002",
				ProviderType:     "service",
				EndpointLocation: "http://localhost",
				BlockIndex:       2,
			},
		},
	}
	g.links = Links{
		{
			From:     g.nodes[1],
			To:       g.nodes[2],
			OutputID: "convertedFile",
			InputID:  "fileIn",
		},
		{
			From:     g.nodes[0],
			To:       g.nodes[1],
			OutputID: "downloadedFile",
			InputID:  "inputFile",
		},
	}
	builder := NewArgoBuilder(&g)
	builder.dataProviderClient = &mockDataProviderClient{}

	expected := v1alpha1.Workflow{
		TypeMeta: v1.TypeMeta{
			Kind:       "Workflow",
			APIVersion: "argoproj.io/v1alpha1",
		},
		ObjectMeta: v1.ObjectMeta{
			GenerateName: "workflow-",
		},
		Spec: v1alpha1.WorkflowSpec{
			ServiceAccountName: "argo-workflow",
			Templates: []v1alpha1.Template{
				{
					Name: "Sentinel-000",
					Outputs: v1alpha1.Outputs{
						Artifacts: v1alpha1.Artifacts{
							{
								Name: "downloadedFile",
								Path: "/mnt/out/image.tif",
							},
						},
					},
					Metadata: v1alpha1.Metadata{
						Annotations: map[string]string{"blockIndex": "0"},
						Labels:      map[string]string{},
					},
					Container: &v1c.Container{
						Image:   "mwendler/wget:latest",
						Command: []string{"sh", "-c"},
						Args:    []string{"cd /mnt/out;wget --tries=1 -O image.tif http://data-provider/image"},
						VolumeMounts: []v1c.VolumeMount{
							{
								Name:      "out",
								MountPath: "/mnt/out",
							},
						},
					},
					Volumes: []v1c.Volume{
						{
							Name: "out",
							VolumeSource: v1c.VolumeSource{
								EmptyDir: &v1c.EmptyDirVolumeSource{
									Medium: "Memory",
								},
							},
						},
					},
				},
				{
					Name: "Converts-an-image-001",
					Inputs: v1alpha1.Inputs{
						Artifacts: v1alpha1.Artifacts{
							{
								Name: "inputFile",
								Path: "/mnt/out/image.tif",
							},
						},
					},
					Outputs: v1alpha1.Outputs{
						Artifacts: v1alpha1.Artifacts{
							{
								Name: "convertedFile",
								Path: "/mnt/out/image.png",
							},
						},
					},
					Metadata: v1alpha1.Metadata{
						Annotations: map[string]string{"blockIndex": "1"},
						Labels:      map[string]string{},
					},
					Container: &v1c.Container{
						Image:   "geodata/gdal",
						Command: []string{"sh", "-c"},
						Args:    []string{"cd /mnt/out;gdal_translate -of PNG /mnt/out/image.tif image.png"},
						VolumeMounts: []v1c.VolumeMount{
							{
								Name:      "out",
								MountPath: "/mnt/out",
							},
						},
					},
					Volumes: []v1c.Volume{
						{
							Name: "out",
							VolumeSource: v1c.VolumeSource{
								EmptyDir: &v1c.EmptyDirVolumeSource{
									Medium: "Memory",
								},
							},
						},
					},
				},
				{
					Name: "Push-To-S3-002",
					Inputs: v1alpha1.Inputs{
						Artifacts: v1alpha1.Artifacts{
							{
								Name: "fileIn",
								Path: "/mnt/out/image.png",
							},
						},
					},
					Metadata: v1alpha1.Metadata{
						Annotations: map[string]string{"blockIndex": "2"},
						Labels:      map[string]string{},
					},
					Container: &v1c.Container{
						Image:   "gcr.io/google.com/cloudsdktool/cloud-sdk:alpine",
						Command: []string{"sh", "-c"},
						Args:    []string{"cd /mnt/out;command /mnt/out/image.png gs://workflow-output/$(date +%s-%N)image.png"},
						VolumeMounts: []v1c.VolumeMount{
							{
								Name:      "out",
								MountPath: "/mnt/out",
							},
						},
					},
					Volumes: []v1c.Volume{
						{
							Name: "out",
							VolumeSource: v1c.VolumeSource{
								EmptyDir: &v1c.EmptyDirVolumeSource{
									Medium: "Memory",
								},
							},
						},
					},
				},
				{
					Name: "dag",
					DAG: &v1alpha1.DAGTemplate{
						Tasks: []v1alpha1.DAGTask{
							{
								Name:         "Sentinel-000",
								Template:     "Sentinel-000",
								Dependencies: []string{},
							},
							{
								Name:     "Converts-an-image-001",
								Template: "Converts-an-image-001",
								Arguments: v1alpha1.Arguments{
									Artifacts: v1alpha1.Artifacts{
										{
											Name: "inputFile",
											From: "{{tasks.Sentinel-000.outputs.artifacts.downloadedFile}}",
										},
									},
								},
								Dependencies: []string{"Sentinel-000"},
							},
							{
								Name:     "Push-To-S3-002",
								Template: "Push-To-S3-002",
								Arguments: v1alpha1.Arguments{
									Artifacts: v1alpha1.Artifacts{
										{
											Name: "fileIn",
											From: "{{tasks.Converts-an-image-001.outputs.artifacts.convertedFile}}",
										},
									},
								},
								Dependencies: []string{"Converts-an-image-001"},
							},
						},
					},
				},
			},
			Entrypoint: "dag",
		},
	}

	dag, err := builder.Build()
	assert.Nil(t, err)
	assert.Equal(t, expected, *dag)
}
