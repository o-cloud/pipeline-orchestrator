package graph

import (
	"fmt"
	"gitlab.com/o-cloud/cwl"
)

func getDockerRequirement(tool *cwl.CommandLineTool) (*cwl.DockerRequirement, error) {
	for _, r := range tool.Requirements {
		switch r.(type) {
		case cwl.DockerRequirement:
			docker := r.(cwl.DockerRequirement)
			return &docker, nil
		}
	}
	return nil, fmt.Errorf("cannot find docker requirement in tool %s", tool.Label)
}
