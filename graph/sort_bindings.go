package graph

import (
	"gitlab.com/o-cloud/cwl"
	values "gitlab.com/o-cloud/pipeline-orchestrator/cwl-values"
	"sort"
)

type bindingWrapper struct {
	binding   cwl.CommandLineBinding
	inputName string
	isInput   bool
	argIndex  int
	value     values.Value
}

type sortableBindings struct {
	bindings []bindingWrapper
}

func (s sortableBindings) Len() int {
	return len(s.bindings)
}

func (s sortableBindings) Swap(i, j int) {
	temp := s.bindings[i]
	s.bindings[i] = s.bindings[j]
	s.bindings[j] = temp
}

func (s sortableBindings) Less(i, j int) bool {
	a := s.bindings[i]
	b := s.bindings[j]
	if a.binding.Position < b.binding.Position {
		return true
	}
	if a.binding.Position > b.binding.Position {
		return false
	}
	if a.isInput && !b.isInput {
		return false
	}
	if !a.isInput && b.isInput {
		return true
	}
	if a.isInput && b.isInput {
		return a.inputName < b.inputName
	}
	if !a.isInput && !b.isInput {
		return a.argIndex < b.argIndex
	}
	return true
}

func SortBindings(tool *cwl.CommandLineTool, inputsValues map[string]values.Value) *[]bindingWrapper {
	bindings := make([]bindingWrapper, len(tool.Arguments))
	for i, arg := range tool.Arguments {
		bindings[i] = bindingWrapper{binding: arg, argIndex: i}
	}
	for key, input := range tool.Inputs {
		if !input.InputBinding.IsFilled {
			continue
		}
		bindings = append(bindings, bindingWrapper{
			binding:   input.InputBinding,
			inputName: key,
			isInput:   true,
			value:     inputsValues[key],
		})
	}
	sortable := sortableBindings{bindings: bindings}
	sort.Sort(sortable)
	return &sortable.bindings
}
