package parallel

import (
	"fmt"
	"log"
	"sync"

	argoLib "github.com/argoproj/argo-workflows/v3/pkg/apis/workflow/v1alpha1"
	"gitlab.com/o-cloud/cwl"
	dataProviderClient "gitlab.com/o-cloud/pipeline-orchestrator/data-provider-client"
)

/**
 * Entry point of the building process
 */

type ParallelArgoBuilder struct {
	graph              *Graph
	wf                 *argoLib.Workflow
	toolsMap           map[*Node]*cwl.CommandLineTool
	dataProviderClient dataProviderClient.DataProviderClient
}

func NewParallelArgoBuilder(graph *Graph) ParallelArgoBuilder {
	return ParallelArgoBuilder{
		graph:              graph,
		toolsMap:           make(map[*Node]*cwl.CommandLineTool),
		dataProviderClient: dataProviderClient.NewDataInserterClient(),
	}
}

func (builder *ParallelArgoBuilder) Build() (*argoLib.Workflow, error) {
	builder.initWorkflow()
	builder.buildParametersFromGraph()
	builder.createTemplate()
	builder.createDags()
	return builder.wf, nil
}

func (builder *ParallelArgoBuilder) initWorkflow() {
	fmt.Println("init workflow")
	w := argoLib.Workflow{}
	w.APIVersion = "argoproj.io/v1alpha1"
	w.Kind = "Workflow"
	w.GenerateName = "workflow-"
	w.Spec = argoLib.WorkflowSpec{
		ServiceAccountName: "argo-workflow",
	}
	builder.wf = &w
}

func (builder *ParallelArgoBuilder) buildParametersFromGraph() {
	log.Println("Building parameters")
	var params []argoLib.Parameter
	var wg sync.WaitGroup
	paramsChan := make(chan argoLib.Parameter)
	for _, node := range builder.graph.nodes {
		wg.Add(1)
		go builder.handleNodeParameters(node, &wg, paramsChan)
	}
	go func() {
		wg.Wait()
		close(paramsChan)
	}()

	for param := range paramsChan {
		params = append(params, param)
	}

	builder.wf.Spec.Arguments = argoLib.Arguments{Parameters: params}
}

func (builder *ParallelArgoBuilder) handleNodeParameters(node *Node, wg *sync.WaitGroup, outParams chan argoLib.Parameter) {
	defer wg.Done()
	if node.EndpointLocation != "" {
		log.Println("Doing provider ", node.name, node.EndpointLocation)

		if parameters, err := getDataFromProvider(node.EndpointLocation, node.Inputs); err != nil {
			log.Println("Error getting data from provider ", node.name, err)
			log.Println("continuing building")
		} else {
			log.Println("Parsing parameters for provider ", node.name, node.EndpointLocation)
			for key, value := range parameters {
				paramName := fmt.Sprintf("%s-%s", node.name, key)
				argoString := argoLib.ParseAnyString(value.GetValue())
				outParams <- argoLib.Parameter{
					Name:  paramName,
					Value: &argoString,
				}
			}
		}
	}
	//Check filled inputs
	for key, value := range node.Inputs {
		outParams <- argoLib.Parameter{
			Name:  fmt.Sprintf("%s-%s", node.name, key),
			Value: argoLib.AnyStringPtr(value.GetValue()),
		}
	}
}
