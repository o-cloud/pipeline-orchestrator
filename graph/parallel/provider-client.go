package parallel

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"
	"time"

	values "gitlab.com/o-cloud/pipeline-orchestrator/cwl-values"
	providerTypes "gitlab.com/o-cloud/provider-library/api"
)

func getDataFromProvider(endpoint string, inputs map[string]values.Value) (map[string]values.Value, error) {

	firstScopeId, err := getRootScopeId(endpoint)

	if err != nil {
		log.Println("Error getting rootscope id at ", endpoint, err)
		return nil, err
	}

	token, err := genearteAccessToken(endpoint, firstScopeId, inputs)

	if err != nil {
		log.Println("Error getting accesstoken at ", endpoint, err)
		return nil, err
	}

	result, err := getProviderData(endpoint, token)

	return result, err

}

func getRootScopeId(endpoint string) (string, error) {
	client := http.Client{}
	//get root acccess

	scopesResult, err := client.Get(endpoint + "/provider/scopes")
	if err != nil {
		return "", err
	}
	var scopes providerTypes.ScopesResponse
	rscope, _ := io.ReadAll(scopesResult.Body)
	err = json.Unmarshal(rscope, &scopes)

	if err != nil {
		return "", err
	}

	if len(scopes.Scopes) == 0 {
		return "", fmt.Errorf("no scopes found")
	}

	firstScopeId := scopes.Scopes[0].Id

	return firstScopeId, nil
}

func genearteAccessToken(endpoint string, scopeId string, inputs map[string]values.Value) (string, error) {

	//Generate access token

	params := make(map[string]interface{})
	for key, value := range inputs {
		params[key] = value.GetValue()
	}

	client := http.Client{}

	accessBody := make(map[string]interface{})
	accessBody["scope"] = scopeId
	accessBody["identities"] = []string{"identity"}
	accessBody["endDate"] = time.Now().Add(time.Hour * 24)
	accessBody["params"] = params

	tokenRequestBody := make(map[string]interface{})
	tokenRequestBody["access"] = accessBody

	tokenRequestBodyJson, _ := json.Marshal(tokenRequestBody)

	tokenResult, err := client.Post(endpoint+"/provider/access/generate-token", "application/json", bytes.NewReader(tokenRequestBodyJson))
	if err != nil {
		log.Println("Error querying generate-token")
		return "", err
	}

	tokenResponse := make(map[string]string)
	rtoken, _ := io.ReadAll(tokenResult.Body)
	json.Unmarshal(rtoken, &tokenResponse)

	token, ok := tokenResponse["token"]
	if !ok {
		log.Println("Error getting token", tokenResponse)
		log.Println("genTokenBody", string(rtoken))
		return "", fmt.Errorf("no token found in genereate token response")
	}

	return token, nil
}

func getProviderData(endpoint string, token string) (map[string]values.Value, error) {

	client := http.Client{}

	accessReq, _ := http.NewRequest("POST", endpoint+"/provider/access", nil)

	accessReq.Header.Add("Authorization", "Bearer "+token)

	r, err := client.Do(accessReq)
	if err != nil {
		return nil, err
	}
	rBytes, err := io.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}
	var responseIntermediaire1 map[string]json.RawMessage
	err = json.Unmarshal(rBytes, &responseIntermediaire1)
	if err != nil {
		log.Println("error unmarshalling access")
		return nil, err
	}
	if _, ok := responseIntermediaire1["access"]; !ok {
		log.Println("responseIntermediaire1", responseIntermediaire1)
		return nil, fmt.Errorf("no access found in response")
	}

	bytesResponseIntermediaire2 := responseIntermediaire1["access"]
	var responseIntermediaire2 map[string]json.RawMessage
	err = json.Unmarshal(bytesResponseIntermediaire2, &responseIntermediaire2)
	if err != nil {
		return nil, err
	}
	if _, ok := responseIntermediaire2["connection"]; !ok {
		return nil, fmt.Errorf("no connection found in access response")
	}
	bytesConnection := responseIntermediaire2["connection"]
	var connection map[string]json.RawMessage
	err = json.Unmarshal(bytesConnection, &connection)
	if err != nil {
		log.Println("error unmarshalling connection", string(bytesConnection))
		return nil, err
	}
	result := make(map[string]values.Value)
	for key, value := range connection {
		strValue := string(value)
		strValue = strings.ReplaceAll(strValue, "%", "%"+"%")
		result[key] = values.StringValue{Value: strValue}
	}
	return result, nil
}
