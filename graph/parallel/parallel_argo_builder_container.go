package parallel

import (
	"fmt"
	"log"
	"reflect"
	"strconv"
	"strings"

	argoLib "github.com/argoproj/argo-workflows/v3/pkg/apis/workflow/v1alpha1"
	"gitlab.com/o-cloud/cwl"
	k8sapiv1 "k8s.io/api/core/v1"
)

/**
 * manage building process of templates with containers
 */

type nodeTemplateBuilder struct {
	ws string
	t  argoLib.Template
	n  *Node
}

func (b *ParallelArgoBuilder) createTemplate() {
	var templates []argoLib.Template
	for _, node := range b.graph.nodes {
		nodeBuilder := nodeTemplateBuilder{n: node, ws: "/mnt/out"}
		nodeBuilder.initTemplate()
		nodeBuilder.manageInputsOutput()
		nodeBuilder.manageArtifact()
		nodeBuilder.buildContainer()
		nodeBuilder.bindCommandInputs()
		templates = append(templates, nodeBuilder.t)
	}
	b.wf.Spec.Templates = templates
}

func (n *nodeTemplateBuilder) initTemplate() {
	log.Println("Init template for node", n.n.name)
	n.t = argoLib.Template{
		Name: n.n.name,
		Inputs: argoLib.Inputs{
			Parameters: []argoLib.Parameter{},
			Artifacts:  []argoLib.Artifact{},
		},
		Outputs: argoLib.Outputs{
			Artifacts: []argoLib.Artifact{},
		},
		Volumes:   []k8sapiv1.Volume{},
		Container: &k8sapiv1.Container{},
		Metadata: argoLib.Metadata{
			Annotations: map[string]string{"blockIndex": strconv.Itoa(n.n.BlockIndex)},
			Labels:      map[string]string{},
		},
	}

}

func (n *nodeTemplateBuilder) manageInputsOutput() {
	log.Println("Manage Inputs")
	for key, inputDef := range n.n.Cwl.Inputs {
		log.Println("Input", key, inputDef)
		// maybe there is a better way to differenciate input types
		if reflect.TypeOf(inputDef.Type.Wrapped) == reflect.TypeOf(cwl.StringType{}) {
			n.t.Inputs.Parameters = append(n.t.Inputs.Parameters, argoLib.Parameter{Name: key})
		}
	}
}

func (n *nodeTemplateBuilder) manageArtifact() {
	createArtifactVolume := false

	//input artifact
	for key, input := range n.n.Cwl.Inputs {
		if reflect.TypeOf(input.Type.Wrapped) == reflect.TypeOf(cwl.FileType{}) {
			createArtifactVolume = true
			n.t.Inputs.Artifacts = append(n.t.Inputs.Artifacts, argoLib.Artifact{
				Name: key,
				Path: fmt.Sprintf("%s/%s", n.ws, key),
			})
		}
	}

	//output artifact
	for key, outputDef := range n.n.Cwl.Outputs {
		if reflect.TypeOf(outputDef.Type.Wrapped) == reflect.TypeOf(cwl.FileType{}) {
			n.t.Outputs.Artifacts = append(n.t.Outputs.Artifacts, argoLib.Artifact{
				Name: key,
				Path: fmt.Sprintf("%s/%s", n.ws, strings.Join(outputDef.Binding.Glob, "-")),
			})
			createArtifactVolume = true
		}
	}

	// Create workspace volume if artifact are present
	if createArtifactVolume {
		n.t.Volumes = append(n.t.Volumes, k8sapiv1.Volume{
			Name: "out",
			VolumeSource: k8sapiv1.VolumeSource{
				EmptyDir: &k8sapiv1.EmptyDirVolumeSource{Medium: k8sapiv1.StorageMediumMemory},
			},
		})
	}
}

func (n *nodeTemplateBuilder) buildContainer() {
	//Command and args
	cwlCommand := n.n.Cwl
	c := n.t.Container
	for _, r := range cwlCommand.Requirements {
		if dockerReq, ok := r.(cwl.DockerRequirement); ok {
			c.Image = dockerReq.DockerPull
			break
		}
	}
	c.Command = []string{"sh", "-c"}
	var args []string
	args = append(args, cwlCommand.BaseCommand...)
	for _, arg := range cwlCommand.Arguments {
		args = append(args, arg.ValueFrom)
	}
	c.Args = []string{"cd " + n.ws + ";" + strings.Join(args, " ")}

	//Volume mount
	vm := []k8sapiv1.VolumeMount{}
	for _, v := range n.t.Volumes {
		vm = append(vm, k8sapiv1.VolumeMount{
			Name:      v.Name,
			MountPath: fmt.Sprintf("/mnt/%s", v.Name),
		})
	}
	if len(vm) > 0 {
		c.VolumeMounts = vm
	}

}

func (n *nodeTemplateBuilder) bindCommandInputs() {
	keyToReplace := make(map[string]string)
	//Extract keys from inputs
	for _, input := range n.t.Inputs.Parameters {
		key := input.Name
		keyToReplace[key] = fmt.Sprintf("{{inputs.parameters.%s}}", key)
	}

	//Extract keys from artifacts
	for _, artifact := range n.t.Inputs.Artifacts {
		key := artifact.Name
		keyToReplace[key] = artifact.Path
	}

	for _, artifact := range n.t.Outputs.Artifacts {
		key := artifact.Name
		keyToReplace[key] = artifact.Path
	}

	//Replace keys in command and args (maybe add more later)
	for key, rep := range keyToReplace {
		formatedKey := fmt.Sprintf(":%s", key)
		for i := range n.t.Container.Command {
			n.t.Container.Command[i] = strings.ReplaceAll(n.t.Container.Command[i], formatedKey, rep)
		}
		for i := range n.t.Container.Args {
			n.t.Container.Args[i] = strings.ReplaceAll(n.t.Container.Args[i], formatedKey, rep)
		}

	}
}
