package parallel

import (
	"fmt"

	argoLib "github.com/argoproj/argo-workflows/v3/pkg/apis/workflow/v1alpha1"
)

func (b *ParallelArgoBuilder) createDags() {

	dagB := dagBuilder{ab: b}

	dagB.setupBuilder()

	dagB.buildNextTask()
	b.wf.Spec.Templates = append(b.wf.Spec.Templates, dagB.dagTemplates...)
}

/** manage the building process around dags and tasks */

type dagBuilder struct {
	ab           *ParallelArgoBuilder
	dagTemplates []argoLib.Template
	dagIndex     int
	taskLinks    map[string][]string
	currentDag   argoLib.Template
	unbuildTasks []string
}

func (db *dagBuilder) setupBuilder() {
	db.dagIndex = 1
	db.currentDag = argoLib.Template{
		Name: "dag-init",
		DAG:  &argoLib.DAGTemplate{},
	}
	db.dagTemplates = append(db.dagTemplates, db.currentDag)
	db.ab.wf.Spec.Entrypoint = db.currentDag.Name
	// 1.1 plan every needed tasks
	for _, t := range db.ab.wf.Spec.Templates {
		db.unbuildTasks = append(db.unbuildTasks, t.Name)
	}

	// 1.2 plan links to do
	db.taskLinks = make(map[string][]string)
	for _, l := range db.ab.graph.links {
		if _, keyExists := db.taskLinks[l.To.name]; !keyExists {
			db.taskLinks[l.To.name] = []string{}
		}
		db.taskLinks[l.To.name] = append(db.taskLinks[l.To.name], l.From.name)
	}
}

func (db *dagBuilder) buildNextTask() {

	nextTaskToBuild := db.determineNextTask()
	if len(nextTaskToBuild) > 2 {
		panic(fmt.Sprintf("Unsupported different task in parallel %s", nextTaskToBuild))
	}
	taskName := nextTaskToBuild[0]
	node, _ := findMatchingNode(db.ab.graph.nodes, taskName)
	//template, _ := findMatchingTemplate(db.ab.wf.Spec.Templates, task)
	// 2.1 if next task has input "WithParam", switch to intermediate Dag
	if inputName, yes := db.isNodeInputWithParam(node); yes {
		//Generate new name for next task
		nextDagName := fmt.Sprintf("dag-%d", db.dagIndex)

		//Create the task with Withparam and correct input
		subDagTask := argoLib.DAGTask{
			Name:      fmt.Sprintf("out-%s", taskName),
			Template:  nextDagName,
			WithParam: formatWorkflowInputParam(taskName, inputName),
			Arguments: argoLib.Arguments{
				Parameters: []argoLib.Parameter{{Name: inputName, Value: argoLib.AnyStringPtr("{{item}}")}},
			},
		}
		db.currentDag.DAG.Tasks = append(db.currentDag.DAG.Tasks, subDagTask)
		//Start the new dag template
		db.currentDag = argoLib.Template{
			Name:   nextDagName,
			DAG:    &argoLib.DAGTemplate{},
			Inputs: argoLib.Inputs{Parameters: []argoLib.Parameter{{Name: inputName}}},
		}
		db.dagTemplates = append(db.dagTemplates, db.currentDag)
		//generate the task and intercept input (template's parameters input, ie "{{inputs.parameters.param}}")
		dagTask := db.buildDagTaskForTask(taskName)
		for i, param := range dagTask.Arguments.Parameters {
			if param.Name == inputName {
				dagTask.Arguments.Parameters[i].Value = argoLib.AnyStringPtr(fmt.Sprintf("{{inputs.parameters.%s}}", inputName))
			}
		}

		db.currentDag.DAG.Tasks = append(db.currentDag.DAG.Tasks, dagTask)
	} else {
		// 3. Build next task from previous input
		dagTask := db.buildDagTaskForTask(taskName)

		db.currentDag.DAG.Tasks = append(db.currentDag.DAG.Tasks, dagTask)
	}

	// 4. Unless list of tasks to build is empty, go back to 2

	db.unbuildTasks = removeFromList(db.unbuildTasks, taskName)
	if len(db.unbuildTasks) == 0 {
		return
	}
	db.buildNextTask()
}

func (db *dagBuilder) isNodeInputWithParam(node Node) (string, bool) {

	for name, input := range node.Cwl.Inputs {
		if findMatch(input.Format, []string{"WithParam"}) {
			return name, true
		}
	}
	return "", false
}

func (db *dagBuilder) determineNextTask() []string {

	nextTaskToBuild := []string{}
	for _, t := range db.unbuildTasks {
		toBuild := true
		if links, ok := db.taskLinks[t]; ok {
			if findMatch(links, db.unbuildTasks) {
				toBuild = false
			}
		}
		if toBuild {
			nextTaskToBuild = append(nextTaskToBuild, t)
		}
	}

	if len(nextTaskToBuild) == 0 && len(db.unbuildTasks) > 0 {
		panic("Couldn't find next task to build, yet there are remaining task in the pipeline")
	}

	return nextTaskToBuild
}

func (db *dagBuilder) determineDependenciesForTask(taskName string) []string {
	if links, ok := db.taskLinks[taskName]; ok {
		return links
	}
	return []string{}
}

func (db *dagBuilder) buildDagTaskForTask(taskName string) argoLib.DAGTask {
	dagTask := argoLib.DAGTask{
		Name:     taskName,
		Template: taskName,
	}
	//Find corresponding template and node (for options ?)
	template, _ := findMatchingTemplate(db.ab.wf.Spec.Templates, taskName)
	//node, _ := findMatchingNode(db.ab.graph.nodes, taskName)

	//Manage template inputs
	for _, input := range template.Inputs.Parameters {
		param := argoLib.Parameter{
			Name:  input.Name,
			Value: argoLib.AnyStringPtr(formatWorkflowInputParam(taskName, input.Name)),
		}
		dagTask.Arguments.Parameters = append(dagTask.Arguments.Parameters, param)
	}

	//Manage artifact inputs
	for _, item := range template.Inputs.Artifacts {
		artifact := argoLib.Artifact{Name: item.Name}
		sourceTask, sourceArtifact, err := db.findArtifactSource(taskName, item.Name)
		if err != nil {
			fmt.Println("Couldn't find source for artifact", taskName, item.Name)
		}
		artifact.From = formatTaskOuputArtifact(sourceTask, sourceArtifact)
		dagTask.Arguments.Artifacts = append(dagTask.Arguments.Artifacts, artifact)
	}

	//Dependency
	dagTask.Dependencies = db.determineDependenciesForTask(taskName)

	return dagTask
}

func (db *dagBuilder) findArtifactSource(taskTarget, inputTarget string) (string, string, error) {
	for _, link := range db.ab.graph.links {
		if link.To.name == taskTarget && link.InputID == inputTarget {
			return link.From.name, link.OutputID, nil
		}
	}
	return "", "", fmt.Errorf("couldn't find source for artifact %s %s", taskTarget, inputTarget)
}

func findMatchingTemplate(templates []argoLib.Template, name string) (argoLib.Template, error) {
	for _, t := range templates {
		if t.Name == name {
			return t, nil
		}
	}
	return argoLib.Template{}, fmt.Errorf("couldn't find template with name %s", name)
}

func findMatchingNode(nodes []*Node, name string) (Node, error) {
	for _, n := range nodes {
		if n.name == name {
			return *n, nil
		}
	}
	return Node{}, fmt.Errorf("couldn't find node with name %s", name)
}

func findMatch(a, b []string) bool {
	for _, aa := range a {
		for _, bb := range b {
			if aa == bb {
				return true
			}
		}
	}
	return false
}

func removeFromList(s []string, r string) []string {
	for i, v := range s {
		if v == r {
			return append(s[:i], s[i+1:]...)
		}
	}
	return s
}

func formatWorkflowInputParam(taskName, inputName string) string {
	return fmt.Sprintf("{{workflow.parameters.%s-%s}}", taskName, inputName)
}

func formatTaskOuputArtifact(sourceTask, sourceArtifact string) string {
	return fmt.Sprintf("{{tasks.%s.outputs.artifacts.%s}}", sourceTask, sourceArtifact)
}
