package graph

import (
	"fmt"
	argoLib "github.com/argoproj/argo-workflows/v3/pkg/apis/workflow/v1alpha1"
	"github.com/sirupsen/logrus"
	"gitlab.com/o-cloud/cwl"
	"gitlab.com/o-cloud/pipeline-orchestrator/argo"
	values "gitlab.com/o-cloud/pipeline-orchestrator/cwl-values"
	dataProviderClient "gitlab.com/o-cloud/pipeline-orchestrator/data-provider-client"
	v1 "k8s.io/api/core/v1"
	"strconv"
	"strings"
)

type ArgoBuilder struct {
	graph              *Graph
	toolsMap           map[*Node]*cwl.CommandLineTool
	dataProviderClient dataProviderClient.DataProviderClient
}

func NewArgoBuilder(graph *Graph) ArgoBuilder {
	return ArgoBuilder{
		graph:              graph,
		toolsMap:           make(map[*Node]*cwl.CommandLineTool),
		dataProviderClient: dataProviderClient.New(),
	}
}

func (builder *ArgoBuilder) Build() (*argoLib.Workflow, error) {
	var templates []argoLib.Template
	for _, node := range builder.graph.nodes {
		//TODO For now workspace will always be /mnt/out which is an empty dir, but we need to provide a way of specifying it in the cwl file
		workspace := "/mnt/out"

		err := builder.addLinkedValuesToInputs(node, workspace)

		if err != nil {
			return nil, err
		}
		if node.ProviderType == "data" {
			err = builder.handleDataNode(node)
			if err != nil {
				return nil, err
			}
		}

		tool := node.Cwl
		builder.toolsMap[node] = &tool
		argoContainer, err := builder.buildArgoContainer(&tool, node, workspace)
		if err != nil {
			return nil, err
		}
		logrus.Debugf("container built %#v", argoContainer)
		outputArtifacts, err := builder.buildOutputArtifacts(&tool, node, workspace, NewExpressionInterpreter(node.Inputs))
		if err != nil {
			return nil, fmt.Errorf("unable to build output artifacts, %s", err)
		}
		inputArtifacts, err := builder.buildInputArtifacts(&tool, node)
		if err != nil {
			return nil, err
		}
		emptyDir := createEmptyDirVolume("out", "Memory")
		argoContainer.VolumeMounts = append(argoContainer.VolumeMounts, v1.VolumeMount{
			Name:      emptyDir.Name,
			MountPath: workspace,
		})
		argoTemplate := argoLib.Template{
			Name:      node.name,
			Container: argoContainer,
			Outputs:   argoLib.Outputs{Artifacts: outputArtifacts},
			Inputs:    argoLib.Inputs{Artifacts: inputArtifacts},
			Volumes:   []v1.Volume{emptyDir},
			Metadata: argoLib.Metadata{
				Annotations: map[string]string{
					"blockIndex": strconv.Itoa(node.BlockIndex),
				},
				Labels: map[string]string{},
			},
		}
		templates = append(templates, argoTemplate)
	}
	dagName := "dag"
	templates = append(templates, builder.buildDag(dagName))
	workflow := argo.NewWorkflow(&templates, dagName)
	return workflow, nil
}

func createEmptyDirVolume(name string, medium v1.StorageMedium) v1.Volume {
	return v1.Volume{
		Name: name,
		VolumeSource: v1.VolumeSource{
			EmptyDir: &v1.EmptyDirVolumeSource{
				Medium: medium,
			},
		},
	}
}

//addLinkedValuesToInputs, for each link pointing to a given node we want to bind the output's value to the input of the node.
//Workspace is the folder in which the command of the node will be executed
func (builder *ArgoBuilder) addLinkedValuesToInputs(node *Node, workspace string) error {
	incomingLinks := findIncomingLinks(builder.graph, node)
	for _, link := range incomingLinks {
		fromTool := builder.toolsMap[link.From]
		if fromTool == nil {
			return fmt.Errorf("error while parsing incoming links, node %s is not parsed yet", link.From.name)
		}
		outputParam, err := findOutputById(fromTool, link.OutputID)
		if err != nil {
			return err
		}
		//TODO handle other types, other glob indexes ...
		switch outputParam.Type.Wrapped.(type) {
		case cwl.FileType:
			node.Inputs[link.InputID] = values.FileValue{Path: workspace + "/" + outputParam.Binding.Glob[0]}
		default:
			return fmt.Errorf("error while parsing incoming links, output type %T is not handled yet", outputParam.Type.Wrapped)
		}
	}
	return nil
}

func findOutputById(tool *cwl.CommandLineTool, id string) (*cwl.CommandOutputParameter, error) {
	parameter, exists := tool.Outputs[id]
	if !exists {
		return nil, fmt.Errorf("output %s doesn't exists in tool %s", id, tool.Label)
	}
	return &parameter, nil
}

func (builder *ArgoBuilder) buildArgoContainer(tool *cwl.CommandLineTool, node *Node, workspace string) (*v1.Container, error) {
	dockerRequirement, err := getDockerRequirement(tool)
	if err != nil {
		return nil, fmt.Errorf("cannot build argo container, %s", err)
	}
	if len(dockerRequirement.DockerPull) == 0 {
		return nil, fmt.Errorf("cannot build argo container for tool %s, docker pull is empty", tool.Label)
	}
	commands, err := getCommands(tool, node.Inputs)
	if err != nil {
		return nil, fmt.Errorf("cannot build argo container for tool %s", tool.Label)
	}
	return &v1.Container{
		Image:   dockerRequirement.DockerPull,
		Command: []string{"sh", "-c"},
		Args:    []string{"cd " + workspace + ";" + strings.Join(commands, " ")},
	}, nil
}

func getCommands(tool *cwl.CommandLineTool, inputsValues map[string]values.Value) ([]string, error) {
	s := SortBindings(tool, inputsValues)
	var commands []string
	commands = append(commands, tool.BaseCommand...)
	for _, binding := range *s {
		line, err := getCommandLine(binding)
		if err != nil {
			return nil, fmt.Errorf("cannot get command line for binding %s", binding.inputName)
		}
		commands = append(commands, line)
	}
	return commands, nil
}

func getCommandLine(binding bindingWrapper) (string, error) {
	command := ""
	if len(binding.binding.Prefix) > 0 {
		command += binding.binding.Prefix
		if binding.binding.Separate {
			command += " "
		}
	}
	if len(binding.binding.ValueFrom) > 0 {
		command += binding.binding.ValueFrom
	} else if binding.value != nil {
		//TODO more types
		switch binding.value.(type) {
		case values.StringValue:
			command += binding.value.(values.StringValue).Value
		case values.IntValue:
			command += strconv.Itoa(binding.value.(values.IntValue).Value)
		case values.FileValue:
			command += binding.value.(values.FileValue).Path
		default:
			return "", fmt.Errorf("unhandled value %t", binding.value)
		}
	}
	return command, nil
}

func (builder *ArgoBuilder) buildOutputArtifacts(tool *cwl.CommandLineTool, node *Node, workspace string, interpreter *ExpressionInterpreter) ([]argoLib.Artifact, error) {

	var outputArtifacts []argoLib.Artifact
	for id, out := range tool.Outputs {
		if outputIsLinked(builder.graph, node, id) {
			glob := out.Binding.Glob[0]
			if IsExpression(glob) {
				//TODO this will break
				tmp, err := interpreter.Interpret(glob)
				if err != nil {
					return nil, fmt.Errorf("unable to interpret expression %s", glob)
				}
				glob = tmp.(values.StringValue).Value
				out.Binding.Glob[0] = glob
			}
			outputArtifacts = append(outputArtifacts, argoLib.Artifact{
				Name: id,
				Path: workspace + "/" + glob,
			})
		}
	}
	return outputArtifacts, nil
}

func (builder *ArgoBuilder) buildInputArtifacts(tool *cwl.CommandLineTool, node *Node) ([]argoLib.Artifact, error) {
	var inputArtifacts []argoLib.Artifact
	for id := range tool.Inputs {
		isLinked := inputIsLinked(builder.graph, node, id)
		if isLinked {
			value := node.Inputs[id]
			if value == nil {
				return nil, fmt.Errorf("error computing input artifacts, can't find value %s", id)
			}
			switch value.(type) {
			case values.FileValue:
				inputArtifacts = append(inputArtifacts, argoLib.Artifact{
					Name: id,
					Path: value.(values.FileValue).Path,
				})
				break
			default:
				return nil, fmt.Errorf("error computing input artifacts, input %s is not a File", id)
			}

		}
	}
	return inputArtifacts, nil
}

func (builder *ArgoBuilder) buildDag(dagName string) argoLib.Template {
	tasks := make([]argoLib.DAGTask, len(builder.graph.nodes))
	for i, node := range builder.graph.nodes {
		var artifacts []argoLib.Artifact
		incomingLinks := findIncomingLinks(builder.graph, node)
		for _, link := range incomingLinks {
			taskArgumentArtifact := argoLib.Artifact{
				Name: link.InputID,
				From: fmt.Sprintf("{{tasks.%s.outputs.artifacts.%s}}", link.From.name, link.OutputID),
			}
			artifacts = append(artifacts, taskArgumentArtifact)
		}
		tasks[i] = argoLib.DAGTask{
			Template:     node.name,
			Name:         node.name,
			Dependencies: incomingLinks.toOriginsName(),
			Arguments:    argoLib.Arguments{Artifacts: artifacts},
		}
	}
	return argoLib.Template{
		Name: dagName,
		DAG:  &argoLib.DAGTemplate{Tasks: tasks},
	}
}

func (builder *ArgoBuilder) handleDataNode(node *Node) error {
	parameters, err := builder.dataProviderClient.ReserveData(node.EndpointLocation+"/reservation", node.Inputs)
	if err != nil {
		return err
	}
	for key, value := range parameters {
		node.Inputs[key] = value
	}
	return nil
}
