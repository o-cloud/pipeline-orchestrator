module gitlab.com/o-cloud/pipeline-orchestrator

go 1.16

require (
	github.com/antonfisher/nested-logrus-formatter v1.3.1
	github.com/argoproj/argo-workflows/v3 v3.1.0
	github.com/gin-gonic/gin v1.7.2
	github.com/google/uuid v1.3.0
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.8.1
	github.com/stretchr/testify v1.7.0
	gitlab.com/o-cloud/cwl v0.0.0-20211021091742-30f22b2b3879
	gitlab.com/o-cloud/provider-library v0.0.0-20220127142930-9b2a8ea87461
	k8s.io/api v0.19.6
	k8s.io/apimachinery v0.19.6
	k8s.io/client-go v0.19.6
)
