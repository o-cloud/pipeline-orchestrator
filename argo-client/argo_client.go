package argo_client

import (
	argoClient "github.com/argoproj/argo-workflows/v3/cmd/argo/commands/client"
	apiWorkflow "github.com/argoproj/argo-workflows/v3/pkg/apiclient/workflow"
	"github.com/argoproj/argo-workflows/v3/pkg/apis/workflow/v1alpha1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type Client struct {
}

func NewClient() *Client {
	return &Client{}
}

func (c *Client) StartWorkflow(workflow v1alpha1.Workflow, namespace string) (*v1alpha1.Workflow, error) {
	ctx, client := argoClient.NewAPIClient()
	wfService := client.NewWorkflowServiceClient()

	wf, err := wfService.CreateWorkflow(ctx, &apiWorkflow.WorkflowCreateRequest{
		Namespace:     namespace,
		Workflow:      &workflow,
		ServerDryRun:  false,
		CreateOptions: &v1.CreateOptions{},
	})
	if err != nil {
		return nil, err
	}
	return wf, nil
}
