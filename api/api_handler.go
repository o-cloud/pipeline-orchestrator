package api

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gitlab.com/o-cloud/pipeline-orchestrator/graph"
	"gitlab.com/o-cloud/pipeline-orchestrator/graph/parallel"
	"gitlab.com/o-cloud/pipeline-orchestrator/models"
	run_controller "gitlab.com/o-cloud/pipeline-orchestrator/run-controller"
)

type Handler struct {
}

func NewHandler() *Handler {
	return &Handler{}
}

func (h *Handler) SetUpRoutes(router *gin.RouterGroup) {
	router.POST("/start", h.startWorkflow)
	router.POST("/startParallel", h.startParallelWorkflow)
}

func (h *Handler) startWorkflow(context *gin.Context) {
	log.Println("received start workflow request")
	var request models.StartWorkflowRequest
	err := context.BindJSON(&request)
	if err != nil {
		logrus.Errorln(err)
		context.Error(err)
		context.Status(http.StatusUnprocessableEntity)
		return
	}
	logrus.Debugf("parsing graph from workflow %#v", request)
	g, err := request.ToGraph()
	if err != nil {
		logrus.Errorln(err)
		context.Error(err)
		context.Status(http.StatusBadRequest)
		return
	}
	logrus.Debugf("building argo from graph %#v", g)
	builder := graph.NewArgoBuilder(g)
	argo, err := builder.Build()
	if err != nil {
		logrus.Errorln(err)
		context.Error(err)
		context.Status(http.StatusBadRequest)
		return
	}

	//TODO handle namespace
	targetNamespace := "workflow"
	logrus.Debugf("starting argo workflow in namespace : %s", targetNamespace)
	argo, err = run_controller.Run(argo, run_controller.RunConfig{
		TargetNamespace: targetNamespace,
	}, request)
	if err != nil {
		logrus.Errorln(err)
		context.Error(err)
		context.Status(http.StatusBadRequest)
		return
	}

	context.JSON(http.StatusOK, models.StartWorkflowResponse{
		Namespace: targetNamespace,
		Workflow:  *argo,
	})
}

func (h *Handler) startParallelWorkflow(context *gin.Context) {
	logrus.Debugln("received start workflow request")
	var request models.StartWorkflowRequest
	err := context.BindJSON(&request)
	if err != nil {
		logrus.Errorln(err)
		context.Error(err)
		context.Status(http.StatusUnprocessableEntity)
		return
	}
	logrus.Debugf("parsing graph from workflow %#v", request)
	graph, err := request.ToParallelGraph()
	if err != nil {
		logrus.Errorln(err)
		context.Error(err)
		context.Status(http.StatusBadRequest)
		return
	}
	logrus.Debugf("building argo from graph %#v", graph)
	builder := parallel.NewParallelArgoBuilder(graph)
	argo, err := builder.Build()
	if err != nil {
		logrus.Errorln(err)
		context.Error(err)
		context.Status(http.StatusBadRequest)
		return
	}

	//TODO handle namespace
	targetNamespace := "workflow"
	logrus.Debugf("starting argo workflow in namespace : %s", targetNamespace)

	argo, err = run_controller.Run(argo, run_controller.RunConfig{
		TargetNamespace: targetNamespace,
	}, request)

	if err != nil {
		logrus.Errorln(err)
		context.Error(err)
		context.Status(http.StatusBadRequest)
		return
	}
	context.JSON(http.StatusOK, models.StartWorkflowResponse{
		Namespace: targetNamespace,
		Workflow:  *argo,
	})
	/**/
}
